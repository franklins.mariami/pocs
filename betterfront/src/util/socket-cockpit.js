import environment from '../../config/environment.json';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';

@inject(EventAggregator)
export class SocketCockpit {

    ea;
    intervalo;
    ws;
    intervalAlive
    constructor(eventAggregator){
        this.ea = eventAggregator;
    }
    
    desligar(){
       /* if(this.intervalo){	
            clearInterval(this.intervalo);	
        }   
        if(this.intervalAlive){	
            clearInterval(this.intervalAlive);	
        }   
        if(this.ws){
            this.ws.close();
            this.ws = null;
        }*/
    }

    checkSocket(){
      /* this.intervalo = window.setInterval(()=>{ 
            if (this.ws != null && this.ws.readyState == WebSocket.CLOSED) {
                console.log('CP - lost connection');
                if(this.intervalAlive){
                    clearInterval(this.intervalAlive);
                }
                this.registrar();
            } 			
        }, 3000);*/
    }

    registrar(){
        /*console.log('environment.socketCockit', environment.socketCockit);
        this.ws = new WebSocket(environment.socketCockit);
        this.ws.onopen = ()=>{
             console.log("CP - SSA");

            this.intervalAlive = window.setInterval(()=>{ 
                if (this.ws != null) {
                    console.log('CP - alive');
                    this.ws.send(""); 
                } 			
            }, 30000);

        };
        this.ws.onmessage = (message)=>{
           this.ea.publish("HANDLE_CP_MESSAGE",{ value: message.data });
        };
        this.ws.onerror = (message)=> {
            console.log('CP: ', message);
            if(this.intervalAlive != null){	
                clearInterval(this.intervalAlive);	
            }   
        };*/
       
    }

    init(){
        /*
        console.log("CP - INIT");
            window.setTimeout(()=>{ 
                console.log("CP client - check");
                this.registrar();
                this.checkSocket();
            }, 1000);
            */
    }
}