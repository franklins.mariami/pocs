
export class StorageUtil  {

  ls=null;

  constructor(){
    /*if(!environment.debug){
      this.ls = new SecureLS({encodingType: 'aes'});
    }*/
  }

  salvarPref(value){
    sessionStorage.setItem('pref', JSON.stringify(value));
  }

  getPref(){
    let pref = sessionStorage.getItem('pref');
    if(pref == null || pref == undefined ){
      return {"ordenacao":null}
    }else{
      return JSON.parse(pref);
    }
  }

  remove(key){
    if(this.ls == null){
      sessionStorage.removeItem(key);
    }else{
      this.ls.remove(key);
    }
  }

  set(key,value){
    if(this.ls == null){
      sessionStorage.setItem(key, JSON.stringify(value));
    }else{
      this.ls.set(key, JSON.stringify(value));
    }
  }

  getFromLocalStorage(key){
    return localStorage.getItem(key);
  }
  
  setInLocalStorage(key,value){
    localStorage.setItem(key, value);
  }

  get(key){
    let value = null;
    if(this.ls == null){
      value = sessionStorage.getItem(key);
    }else{
     value = this.ls.get(key);
    }
    if(value){
      return JSON.parse(value);
    }
    return null;
  }

  clear(){
      if(this.ls != null){
        this.ls.removeAll();
      }else{
        sessionStorage.clear();
      }
  }
}
