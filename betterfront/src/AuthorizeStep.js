import {Redirect} from 'aurelia-router';
export class AuthorizeStep {

   
  constructor(session){
    this.session = session;
  }

   run(navigationInstruction, next) {
      if (navigationInstruction.getAllInstructions().some(i => i.config.settings.auth)) {

      let user = this.session.get('user');
      if (user == null) {
        return next.cancel(new Redirect('login'));
      }else{
        return next();
      }
     } 
     return next();
   }
}
