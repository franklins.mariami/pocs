import {Router} from 'aurelia-router';
import {inject} from 'aurelia-framework';
import {PLATFORM} from 'aurelia-pal';
import {AuthorizeStep} from 'AuthorizeStep';
import {StorageUtil} from "util/storage-util";
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(Router,StorageUtil,EventAggregator)
export class App {

  subscriberExpire
  constructor(router, storageUtil, eventAggregator) {
    this.router = router;
    this.storageUtil = storageUtil;
    this.ea = eventAggregator;
  }

  detached(){
    if(this.subscriberServico){
      this.subscriberServico.dispose();
      this.subscriberServico = null;
    }
  }

  attached(){
    this.subscriberServico = this.ea.subscribe('SESSION_EVT', (code)=>{
      this.processarSessionEvt(code);
    });
  }
  
  processarSessionEvt(v){
    if(v.code == "FORBIDDEN"){
      this.router.navigate('/forbidden');
    }
    if(v.code == "EXPIRED"){
      this.router.navigate('/expired');
    }
  }

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Cockpit';
    const step = new AuthorizeStep(this.storageUtil);
    config.addAuthorizeStep(step);
 
    config.map([
      { route: '', redirect:'login'},
      { route: ['login'], name: 'login', moduleId: PLATFORM.moduleName('views/login/login'), title: 'Login'},
      { route: ['home'], name: 'home', moduleId: PLATFORM.moduleName('views/home/home'), title: 'Cockpit', settings: { auth: true }},
      { route: ['forbidden'], name: 'forbidden', moduleId: PLATFORM.moduleName('views/forbidden/forbidden'), title: 'Login', settings: { auth: false }},
      { route: ['expired'], name: 'expired', moduleId: PLATFORM.moduleName('views/expire/expire'), title: 'Login', settings: { auth: false }},
      { route: ['end-test'], name: 'teste', moduleId: PLATFORM.moduleName('views/teste/teste'), title: 'Test', settings: { auth: false }},
      { route: ['client-home'], name: 'client-home', moduleId: PLATFORM.moduleName('views/cliente/dashboard/client-dashboard'), title: 'Cliente', settings: { auth: true }},
      { route: ['login-cockpit-du'], name: 'login-cockpit-du', moduleId: PLATFORM.moduleName('views/cliente/login/dutrade/cockpit/cockpit'), title: 'Login', settings: { auth: false }},
      { route: ['login-cliente-du'], name: 'login-cliente-du', moduleId: PLATFORM.moduleName('views/cliente/login/dutrade/cliente/login'), title: 'Login', settings: { auth: false }},
      { route: ['cadastro-cockpit-du'], name: 'cadastro-cockpit-du', moduleId: PLATFORM.moduleName('views/cadastro/cockpit/dutrade/cockpit'), title: 'Cadastro', settings: { auth: false }},
      { route: ['cadastro-cliente-du'], name: 'cadastro-cliente-du', moduleId: PLATFORM.moduleName('views/cadastro/cliente/dutrade/cliente'), title: 'Cadastro', settings: { auth: false }},
      { route: ['rtip'], name: 'rtip', moduleId: PLATFORM.moduleName('views/atualizar/atualizar'), title: 'atualizar', settings: { auth: false }},
     
    ]);
    config.mapUnknownRoutes({ redirect: 'nao-encontrado' });
  }

}
