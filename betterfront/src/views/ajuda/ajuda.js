import {BasicView} from 'views/BasicView';
import Swal from 'sweetalert2';

export class Ajuda extends BasicView {

  duvida={"titulo":'',"descricao":""}
  constructor(...rest){
    super(...rest);
  }  
  
  voltar(){
    this.navigate("panel");
  }

  async enviarDuvida(){
    if(this.duvida.titulo == '' || this.duvida.descricao == ''){
      Swal.fire({
        title: 'Dados inválidos',
        'text':'Preencha corretamente o formulário para enviar.', 
        confirmButtonText: 'Fechar',
      });
      return;
    }

    $('#container').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
  
    let retorno = await this.service.enviarDuvida(this.duvida);
    $('#container').unblock(); 

    if(retorno.status.code == 1){
      Swal.fire({
        title: 'Dúvida enviada',
        html:'Obrigado por entrar em contato! <br></br>Em breve retornaremos o seu contato.', 
        confirmButtonText: 'Fechar',
      });
    }
  }
}
