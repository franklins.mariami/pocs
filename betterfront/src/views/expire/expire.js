import {BasicView} from 'views/BasicView';

export class Expire extends BasicView {
  constructor(...rest){
    super(...rest);
  }  

  attached(){
    $('.modal-backdrop.fade.show').remove();
  }
  
  onVoltarLogin(){
    this.navigate("login");
  }
}
