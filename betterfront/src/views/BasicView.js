import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {Service} from 'services/service';
import {StorageUtil} from "util/storage-util";
import jwt_decode from "jwt-decode";
import {I18N} from 'aurelia-i18n';

@inject(Router, Service, StorageUtil,I18N)
export class BasicView {

  constructor(router,service,storageUtil,I18N){
    this.router = router;
    this.service = service;
    this.session = storageUtil;
    this.i18n = I18N;
  }
  
  unblock(id){
    $('#' + id).unblock(); 
  }
  
  block(id){
    $('#' + id).block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    });
  }

  getClientProperty(value){
    let token = this.getCliente().token;
    let decoded = jwt_decode(token);
    return decoded[value];
  }

  getProperty(value){
    let token = this.getUsuario().token;
    let decoded = jwt_decode(token);
    return decoded[value];
  }

  isUserInRole(role){
    let token = this.getUsuario().token;
    let decoded = jwt_decode(token);
    return decoded.roles.indexOf(role) != -1;
  }

  salvarPref(value){
    return this.session.salvarPref(value);
  }

  getPref(){
    return this.session.getPref();
  }

  setSession(key,value){
    this.session.set(key,value);
  }

  getInplayDate(){
    return this.session.get("inplay-date");
  }

  getCliente(){
    return this.session.get('client');
  }

  getUsuario(){
    return this.session.get('user');
  }

  removeSession(key){
    this.session.remove(key);
  }
  
  navigate(value){
    this.router.navigate(value);
  }

  navigateParam(value, param){
    this.router.navigate(value + '/' + param);
  }

  onCheckWindowWidth = () => {
    const width  = window.innerWidth || document.documentElement.clientWidth || 
    document.body.clientWidth;
    return (width > 860) ? true : false;
  }
}  
