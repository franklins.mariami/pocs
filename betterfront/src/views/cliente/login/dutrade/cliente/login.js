import {BasicView} from 'views/BasicView';

export class Login extends BasicView {
 
  user={login:"", senha:""}
  loginInvalido = false;
  mensagemErroLogin="";
  constructor(...rest){
    super(...rest);
  }  
  
  attached(){
    super.removeSession('user');
    setTimeout(()=> {$('#login').focus();},10);
    var _this  = this;

    $('.validate-input .input100').each(function(){
      $(this).on('blur', function(){
          if(_this.validate(this) == false){
            _this.showValidate(this);
          }
          else {
              $(this).parent().addClass('true-validate');
          }
      })    
    });

    $('.validate-form .input100').each(function(){
      $(this).focus(function(){
         _this.hideValidate(this);
         $(this).parent().removeClass('true-validate');
      });
    });
  }     

  passwordFocus(){
    $('#senha').focus();
  }

  validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

  showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');

    $(thisAlert).append('<span class="btn-hide-validate">&#xf135;</span>')
    $('.btn-hide-validate').each(()=>{
        $(this).on('click',()=>{
           this.hideValidate(this);
        });
    });
} 

hideValidate(input) {
  var thisAlert = $(input).parent();
  $(thisAlert).removeClass('alert-validate');
  $(thisAlert).find('.btn-hide-validate').remove();
}

async login(){
    this.mensagemErroLogin = "";
    this.loginInvalido = false;
    this.hideValidate($("#login"));
    this.hideValidate($("#senha"));

    if(this.validate($("#login")) == false){
      this.showValidate($("#login"));
    } 
    if(this.validate($("#senha")) == false){
      this.showValidate($("#senha"));
    }
    
    this.loginInvalido = false;
    if(this.user.login =='' || this.user.senha =='' ){
      this.loginInvalido = true;
    }else { 
      $('#container').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      }); 
      let retorno = await this.service.loginCliente(this.user.login,this.user.senha);
      $('#container').unblock(); 
      if(retorno.status.code ==1){
        this.setSession('user', retorno.value);
        this.navigate('client-home');
      }else{
        if(retorno.status.errorCode == 1){
          this.navigate("/expirado");
        }else{
          this.mensagemErroLogin = retorno.status.message;
          this.loginInvalido = true;
        }
      } 
    }
  }
}
 