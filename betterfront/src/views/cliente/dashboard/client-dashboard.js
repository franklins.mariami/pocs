import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {Service} from 'services/service';
import {PLATFORM} from 'aurelia-pal';
import {BasicView} from 'views/BasicView';
import {EventAggregator} from 'aurelia-event-aggregator';
import {StorageUtil} from "util/storage-util";

@inject(Service,Router,EventAggregator,StorageUtil)
export class ClientDashboard extends BasicView {

  constructor(service,router, eventAggregator, storageUtil, ...rest){
    super(...rest);
    this.service = service;
    this.router = router;
    this.ea = eventAggregator;
    this.storageUtil = storageUtil;
  }  
  
  attached(){}

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Cockpit';
    
    config.map([
      { route: '', redirect:'dash'},
      { route: ['dash'], name:'dash', title: 'Cliente Dashboard',  moduleId: PLATFORM.moduleName('./views/client-main/client-main'), settings: { auth: false }},
      
    ]);
    config.mapUnknownRoutes({ redirect: 'nao-encontrado' });
  }

  onLogout = () => {
    this.navigate("/");
  }

  abrirDuvida(){
    this.navigate("ajuda");
  }
}
