import {BasicView} from 'views/BasicView';
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {DialogService} from 'aurelia-dialog';
import {Chart} from '../../../../../../node_modules/chart.js/dist/Chart.js';
import flatpickr from "flatpickr";
import { Portuguese } from "flatpickr/dist/l10n/pt.js"

@inject(EventAggregator, DialogService)
export class ClientMain extends BasicView {
  editing=false;
  lucroDiaDate=new Date();
  initialDate=new Date();
  finalDate=new Date();
  status={};

  chartLabels = [];
  chartValues = [];

  constructor(eventAggregator, dialogService, ...rest){
    super(...rest); 
    this.ea = eventAggregator;
    this.dialogService = dialogService;
  }

  attached() {
    this.onConfigForm();
    this.onSetDates();
    this.fillDatePicker();
    this.buscarStatus();
  }
 
  async buscarStatus(){
    
    $('#mainArea').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
    let retorno = await this.service.buscarStatusCliente({data:null});
    $('#mainArea').unblock(); 

    if(retorno.status.code == 1){
      this.status = retorno.value;
      this.status.entradas.forEach(e=>{
        this.chartLabels.push(e.hora); 
        this.chartValues.push(parseFloat(e.valor));
      })
      this.onSetCharts();
    }
  }

  onSetCharts = () => {
    /* chart.js chart examples */

    // chart colors
    var colors = ['#79a3fd82','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

    /* large line chart */
    var chLine = document.getElementById("chLine");
    var chartData = {
      labels: this.chartLabels,
      datasets: [{
        data: this.chartValues,
        label: 'Lucro diário',
        backgroundColor: 'transparent',
        borderColor: colors[0],
        borderWidth: 1,
        pointBackgroundColor: colors[1]
      }
      ]
    };
    if (chLine) {
      let test = new Chart(chLine, {
      type: 'line',
      data: chartData,
      options: {
        scales: { 
          x: {
            display: true,
            title: {
              display: true,
              text: 'Horário de entrada',
            }
          },
          y: {
            display: true,
            title: {
              display: true,
              text: 'Meu Capital',
            }
          },
          xAxes: [{
            ticks: {
              beginAtZero: true,
            },
            title: {
              display: true,
              text: 'Meu Capital',
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true,
      }
      });
    }

    /* large pie/donut chart */
    var chPie = document.getElementById("chPie");
    if (chPie) {
      new Chart(chPie, {
        type: 'pie',
        data: {
          labels: ['Desktop', 'Phone', 'Tablet', 'Unknown'],
          datasets: [
            {
              backgroundColor: [colors[1],colors[0],colors[2],colors[5]],
              borderWidth: 0,
              data: [50, 40, 15, 5]
            }
          ]
        },
        plugins: [{
          beforeDraw: function(chart) {
            var width = chart.chart.width,
                height = chart.chart.height,
                ctx = chart.chart.ctx;
            ctx.restore();
            var fontSize = (height / 70).toFixed(2);
            ctx.font = fontSize + "em sans-serif";
            ctx.textBaseline = "middle";
            var text = chart.config.data.datasets[0].data[0] + "%",
                textX = Math.round((width - ctx.measureText(text).width) / 2),
                textY = height / 2;
            ctx.fillText(text, textX, textY);
            ctx.save();
          }
        }],
        options: {layout:{padding:0}, legend:{display:false}, cutoutPercentage: 80}
      });
    }

    /* bar chart */
    var chBar = document.getElementById("chBar");
    if (chBar) {
      new Chart(chBar, {
      type: 'bar',
      data: {
        labels: ["S", "M", "T", "W", "T", "F", "S"],
        datasets: [{
          data: [589, 445, 483, 503, 689, 692, 634],
          backgroundColor: colors[0]
        },
        {
          data: [639, 465, 493, 478, 589, 632, 674],
          backgroundColor: colors[1]
        }]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            barPercentage: 0.4,
            categoryPercentage: 0.5
          }]
        }
      }
      });
    }

  }

  onConfigForm = () => {
    const forms = document.querySelectorAll('.requires-validation')
    Array.from(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }
    
          form.classList.add('was-validated')
        }, false)
      })
  }

  onSetDates = () => {
    this.ajustarDia(this.lucroDiaDate);
    this.ajustarDia(this.initialDate);
    this.ajustarDia(this.finalDate);
  }

  fillDatePicker = (selected = new Date()) => {
    this.datePickerInitial = flatpickr("#clientInitialDate", {
      altInput: true,
      altFormat: "F j, Y",
      dateFormat: "d-m-Y",
      defaultDate: selected,
      disableMobile: true,
      static: this.onCheckWindowWidth(),
      "locale": Portuguese,
      onChange: (selectedDate, dateStr, instance) => {
        this.initialDate = selectedDate;
        // this.setSession('inplay-date',this.dataSelecionada);
        // this.buscarLucro();
      }
    });

    this.datePickerFinal = flatpickr("#clientFinalDate", {
      altInput: true,
      altFormat: "F j, Y",
      dateFormat: "d-m-Y",
      defaultDate: selected,
      disableMobile: true,
      static: this.onCheckWindowWidth(),
      "locale": Portuguese,
      onChange: (selectedDate, dateStr, instance) => {
        this.finalDate = selectedDate;
        // this.setSession('inplay-date',this.dataSelecionada);
        // this.buscarLucro();
      }
    });


    this.datePickerLucroDia = flatpickr("#lucroDiaDate", {
      altInput: true,
      altFormat: "F j, Y",
      dateFormat: "d-m-Y",
      defaultDate: selected,
      disableMobile: true,
      static: this.onCheckWindowWidth(),
      "locale": Portuguese,
      onChange: (selectedDate, dateStr, instance) => {
        this.lucroDiaDate = selectedDate;
        // this.setSession('inplay-date',this.dataSelecionada);
        // this.buscarLucro();
      }
    });
  }

  onOpendDatePicker = (type="initial") => {
    switch (type) {
      case "initial":
        this.datePickerInitial.open();
        break;
      case "final":
        this.datePickerFinal.open();
        break;
      case "lucro-dia":
        this.datePickerLucroDia.open();
        break;
      default:
        break;
    }
  }

  ajustarDia(newDate = new Date()){
    var selectedDate = new Date(newDate);
    var dd = String(selectedDate.getDate()).padStart(2, '0');
    var mm = String(selectedDate.getMonth() + 1).padStart(2, '0');  
    var yyyy = selectedDate.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
  }

  onToggleEditing = () => {
    this.editing = !this.editing;
  }

}
