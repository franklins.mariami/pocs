import {BasicView} from 'views/BasicView';
import { SocketCockpit } from '../../util/socket-cockpit';
import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {DialogService} from 'aurelia-dialog';
import Swal from 'sweetalert2';

@inject(EventAggregator, DialogService, SocketCockpit)
export class Cockpit  extends BasicView {
  partidas;
  today;
  fullToday=[];
  time;
  medias=[];
  mediaEscolhida="";
  ultimaAtualizacao = "";
  user = null;
  inplay=[];
  lucro=[];
  contas=[];
  listaUsuarioCopyTip=[];
  operador;
  idMercado;
  partida;
  contasCopy=[];
  selecao="";
  mensagem="Selecione o mercado desejado.";
  mensagemEntrada="";
  onEditing = false;
  porcentagem="";
  odd="";
  partidasPotenciais=[];
  login;
  partidaTelegram;
  opcoesTelegram=[];
  gruposTelegramSelecionado=[];
  saldoConta = 0;
  valorEntrada = 0;
  lucroEntrada = 0;
  porcentagemCopyTip = "";
  selecionarContasCopy=false;
  oddsOptions = [
    { title: '0,5', control: '05', show: true },
    { title: '1,5', control: '15', show: true },
    { title: '2,5', control: '25', show: true },
    { title: '3,5', control: '35', show: true },
    { title: '4,5', control: '45', show: true },
    { title: '5,5', control: '55', show: true },
    { title: '6,5', control: '65', show: true },
    { title: '7,5', control: '75', show: true },
    { title: '8,5', control: '85', show: true },
  ]
  selectedRow = null;
  percentualEntradaGeral = "";
  tabs = {
    tab1: true,
    tab2: false,
    tab3: false,
    tab4: false,
    tab5: false,
  }
  mostrarPrimeiroTempo=false;
  opcaoMercado = [
    { title: '0,5',idMercado:"05", operador:"-", control: '05_UNDER', show: true, under:true, ht:false, label:"Menos de 0,5 Gols" },
    { title: '1,5',idMercado:"15", operador:"-", control: '15_UNDER', show: true, under:true, ht:false, label:"Menos de 1,5 Gols" },
    { title: '2,5',idMercado:"25", operador:"-", control: '25_UNDER', show: true, under:true, ht:false, label:"Menos de 2,5 Gols" },
    { title: '3,5',idMercado:"35", operador:"-", control: '35_UNDER', show: true, under:true, ht:false, label:"Menos de 3,5 Gols" },
    { title: '4,5',idMercado:"45", operador:"-", control: '45_UNDER', show: true, under:true, ht:false, label:"Menos de 4,5 Gols" },
    { title: '5,5',idMercado:"55", operador:"-", control: '55_UNDER', show: true, under:true, ht:false, label:"Menos de 5,5 Gols" },
    { title: '6,5',idMercado:"65", operador:"-", control: '65_UNDER', show: true, under:true, ht:false, label:"Menos de 6,5 Gols" },
    { title: '7,5',idMercado:"75", operador:"-", control: '75_UNDER', show: true, under:true, ht:false, label:"Menos de 7,5 Gols" },
    { title: '8,5',idMercado:"85", operador:"-", control: '85_UNDER', show: true, under:true, ht:false, label:"Menos de 8,5 Gols" },
    { title: '0,5',idMercado:"05", operador:"+", control: '05_OVER', show: true, under:false, ht:false, label:"Mais de 0,5 Gols" },
    { title: '1,5',idMercado:"15", operador:"+", control: '15_OVER', show: true, under:false, ht:false, label:"Mais de 1,5 Gols" },
    { title: '2,5',idMercado:"25", operador:"+", control: '25_OVER', show: true, under:false, ht:false, label:"Mais de 2,5 Gols" },
    { title: '3,5',idMercado:"35", operador:"+", control: '35_OVER', show: true, under:false, ht:false, label:"Mais de 3,5 Gols" },
    { title: '4,5',idMercado:"45", operador:"+", control: '45_OVER', show: true, under:false, ht:false, label:"Mais de 4,5 Gols" },
    { title: '5,5',idMercado:"55", operador:"+", control: '55_OVER', show: true, under:false, ht:false, label:"Mais de 5,5 Gols" },
    { title: '6,5',idMercado:"65", operador:"+", control: '65_OVER', show: true, under:false, ht:false, label:"Mais de 6,5 Gols" },
    { title: '7,5',idMercado:"75", operador:"+", control: '75_OVER', show: true, under:false, ht:false, label:"Mais de 7,5 Gols" },
    { title: '8,5',idMercado:"85", operador:"+", control: '85_OVER', show: true, under:false, ht:false, label:"Mais de 8,5 Gols" },
 
    { title: '0,5', idMercado:"05t", operador:"-", control: '05_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 0,5 Gols" },
    { title: '1,5', idMercado:"15t", operador:"-", control: '15_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 1,5 Gols" },
    { title: '2,5', idMercado:"25t", operador:"-", control: '25_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 2,5 Gols" },
    { title: '3,5', idMercado:"35t", operador:"-", control: '35_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 3,5 Gols" },
    { title: '4,5', idMercado:"45t", operador:"-", control: '45_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 4,5 Gols" },
    { title: '5,5', idMercado:"55t", operador:"-", control: '55_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 5,5 Gols" },
    { title: '6,5', idMercado:"65t", operador:"-", control: '65_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 6,5 Gols" },
    { title: '7,5', idMercado:"75t", operador:"-", control: '75_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 7,5 Gols" },
    { title: '8,5', idMercado:"85t", operador:"-", control: '85_UNDER', show: true, under:true, ht:true,  label:"1° Tempo - Menos de 8,5 Gols" },
    { title: '0,5', idMercado:"05t", operador:"+", control: '05_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 0,5 Gols" },
    { title: '1,5', idMercado:"15t", operador:"+", control: '15_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 1,5 Gols" },
    { title: '2,5', idMercado:"25t", operador:"+", control: '25_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 2,5 Gols" },
    { title: '3,5', idMercado:"35t", operador:"+", control: '35_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 3,5 Gols" },
    { title: '4,5', idMercado:"45t", operador:"+", control: '45_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 4,5 Gols" },
    { title: '5,5', idMercado:"5t",  operador:"+", control: '55_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 5,5 Gols" },
    { title: '6,5', idMercado:"65t", operador:"+", control: '65_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 6,5 Gols" },
    { title: '7,5', idMercado:"75t", operador:"+", control: '75_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 7,5 Gols" },
    { title: '8,5', idMercado:"85t", operador:"+", control: '85_OVER', show: true, under:false, ht:true,  label:"1° Tempo - Mais de 8,5 Gols" },
    
  ]
  
  opcaoTelegramSelecionadaOver;
  opcaoTelegramSelecionadaUnder;
  opcaoTelegramSelecionadaOverHT;
  opcaoTelegramSelecionadaUnderHT;
  listaGols=[];
  totalGolsSelecionado="-";

  ordenacao = [
    { id: 0, name: 'Tempo' },
    { id: 3, name: 'Placar 0x0' }
  ];
  ordenacaoSelecionada;
  
  intervalAtualizacao;
  intervalDados;
  selectAll=false;
  partidaSelecionada=null;
  avisarTipCockpit=false;
  vitoriasCasa = 0;
  vitoriasVisitante = 0;
  empates=0;
  listaOvers=[];
  listaUnders=[];
  listaOversHt=[];
  listaUndersHt=[];

  constructor(eventAggregator, dialogService, socketCockpit, ...rest){
    super(...rest); 
    this.ea = eventAggregator;
    this.dialogService = dialogService;
    this.socketCockpit = socketCockpit;
  } 

  attached(){
    this.user = this.getUsuario();
    this.preferencias = this.getPref();

    var today = new Date();  
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');  
    var yyyy = today.getFullYear();
    this.today = dd + '/' + mm + '/' + yyyy;
    
    this.pesquisar(this.today,false); 
    //this.startLiveUpdate();
    //this.intervalRemoverFinalizadas();
    
    //this.subscribeCockpitSocket();
    //this.atualizarConfig(); 
    //this.ajustarFiltroOver();
    

    setInterval(() => {
        this.pesquisarSilent(this.today,false); 
    }, 15000);
  } 
 

  abrirDetalhesPartida(partida){
    this.partidaSelecionada = partida;
    if(this.partidaSelecionada.confrontos && this.partidaSelecionada.confrontos.length > 0){
      this.vitoriasCasa = 0;
      this.vitoriasVisitante = 0;
      this.empates=0;

      this.partidaSelecionada.confrontos.forEach(c=>{
        if(parseInt(c.goals_home) == parseInt(c.goals_away)){
          this.empates+=1;
        }else{
          if(parseInt(c.goals_home) > parseInt(c.goals_away)){
            c.cssCasa="css-time-vitoria";
          }else if(parseInt(c.goals_home) < parseInt(c.goals_away)){
            this.vitoriasVisitante+=1;
            c.cssVisitante="css-time-vitoria";
          }
        }
      });
      if(this.vitoriasCasa>0){
        this.vitoriasCasa = parseInt((this.vitoriasCasa  / this.partidaSelecionada.confrontos.length ) *  100);
      }
      if(this.vitoriasVisitante>0){
        this.vitoriasVisitante = parseInt((this.vitoriasVisitante  / this.partidaSelecionada.confrontos.length ) *  100);
      }
      if(this.empates>0){
        this.empates = parseInt((this.empates  / this.partidaSelecionada.confrontos.length ) *  100);
      }

      let modalTelegram = new bootstrap.Modal(document.getElementById('modal-detalhes-partida'), {});
      modalTelegram.show();
    }
    if(partida.overs.length > 0){

      this.listaOvers=[];
      this.listaUnders=[];
      this.listaOversHt=[];
      this.listaUndersHt=[];

      partida.overs.forEach(o=>{

        if(o.mercado == "over05g_ht"){
          this.listaOversHt.push({mercado:"0.5",value:o.valor});
        }
        if(o.mercado == "over15g_ht"){
          this.listaOversHt.push({mercado:"1.5",value:o.valor});
        }
        if(o.mercado == "over25g_ht"){
          this.listaOversHt.push({mercado:"2.5",value:o.valor});
        }
        if(o.mercado == "over35g_ht"){
          this.listaOversHt.push({mercado:"3.5",value:o.valor});
        }
        if(o.mercado == "over45g_ht"){
          this.listaOversHt.push({mercado:"4.5",value:o.valor});
        }

        
        if(o.mercado == "under05g_ht"){
          this.listaUndersHt.push({mercado:"0.5",value:o.valor});
        }
        if(o.mercado == "under15g_ht"){
          this.listaUndersHt.push({mercado:"1.5",value:o.valor});
        }
        if(o.mercado == "under25g_ht"){
          this.listaUndersHt.push({mercado:"2.5",value:o.valor});
        }
        if(o.mercado == "under35g_ht"){
          this.listaUndersHt.push({mercado:"3.5",value:o.valor});
        }
        if(o.mercado == "under45g_ht"){
          this.listaUndersHt.push({mercado:"4.5",value:o.valor});
        }

        if(o.mercado == "over05g"){
          this.listaOvers.push({mercado:"0.5",value:o.valor});
        }
        if(o.mercado == "over15g"){
          this.listaOvers.push({mercado:"1.5",value:o.valor});
        }
        if(o.mercado == "over25g"){
          this.listaOvers.push({mercado:"2.5",value:o.valor});
        }
        if(o.mercado == "over35g"){
          this.listaOvers.push({mercado:"3.5",value:o.valor});
        }
        if(o.mercado == "over45g"){
          this.listaOvers.push({mercado:"4.5",value:o.valor});
        }

        
        if(o.mercado == "under05g"){
          this.listaUnders.push({mercado:"0.5",value:o.valor});
        }
        if(o.mercado == "under15g"){
          this.listaUnders.push({mercado:"1.5",value:o.valor});
        }
        if(o.mercado == "under25g"){
          this.listaUnders.push({mercado:"2.5",value:o.valor});
        }
        if(o.mercado == "under35g"){
          this.listaUnders.push({mercado:"3.5",value:o.valor});
        }
        if(o.mercado == "under45g"){
          this.listaUnders.push({mercado:"4.5",value:o.valor});
        }

      })
    }
  }
   
  getCssRowConfronto(index){
   if(index % 2 == 0 ){
    return "container-confronto-container-even";
   }else{
    return "container-confronto-container-odd";
   }
  } 

  ajustarFiltroOver(){
    this.listaGols = [];
    this.listaGols.push("-");  
    this.listaGols.push("1"); 
    this.listaGols.push("2");
    this.listaGols.push("3");
    this.listaGols.push("4");
    this.listaGols.push("5");
    this.listaGols.push("6");
    this.listaGols.push("7");
  }

  aplicarFiltroOver(){
    if(this.partidas){
      this.partidas.forEach(p=>{
        this.checarVisibilidateFiltro(p);
      });
    }
  }

  checarVisibilidateFiltro(p){
    let media = 0;
    if(p.golsInicial != null && p.golsInicial != '' ){
      media = parseFloat(p.golsInicial,10);
    }
    if(this.totalGolsSelecionado == "Todos" ) {
      p.visivel = true;
    }else{
      p.visivel = media == parseFloat(this.totalGolsSelecionado);
    }

  }

  fecharModalEntrada(){
    $('#btnFecharEntrada').click();
  }

  async realizarEntrada(){
    this.mensagemEntrada = "";
    if(this.idMercado =="" || this.idMercado == null){
      Swal.fire({
        title: 'Realizar entrada',
        html:"Selecione o <strong>mercado</strong> desejado para realizar a entrada.", 
        confirmButtonText: 'Fechar',
      });
      return 
    }
    if(this.user.percEntrada == "" || this.user.percEntrada == 0 ){
      Swal.fire({
        title: 'Realizar entrada',
        html:"Selecione o <strong>percentual de entrada</strong> para concluir sua entrada.", 
        confirmButtonText: 'Fechar',
      });
      return
    }
    let valorEntrada = parseFloat((this.saldoConta * this.user.percEntrada)/100).toFixed(2)

    if(valorEntrada < 5){
      Swal.fire({
        title: 'Realizar entrada',
        html:"O valor <strong>mínimo</strong> de entrada é de <strong>R$ 5,00</strong> reais. <strong>Aumente</strong> um pouco seu percentual de entrada e confirme a entrada.", 
        confirmButtonText: 'Fechar',
      });
      return
    }

    $('#container-entrada').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
 
    let retorno = await this.service.entradaManual({
        "idPartidaBetFair":this.partida.idPartidaBetFair,
        "idMercado":this.idMercado,
        "operador":this.operador,
        "login":null,
        "odd":this.odd,
        "percentualEntrada":this.user.percEntrada,
        "mercado":this.mercado
    });  
    if(retorno.status.code == 1){
      Swal.fire({
        title: 'Entrada realizada',
        'text':"Sua entrada foi realizada com sucesso.", 
        confirmButtonText: 'Fechar',
      });
      this.fecharModalEntrada();  
    }else{
      Swal.fire({
        title: 'Atenção',
        'text':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
    this.mensagemEntrada = retorno.status.message;
    this.selecao = "";
    $('#container-entrada').unblock(); 
   
  }

  selectMatcher = (a, b) => {
    if(a != undefined && b != undefined){
     return a.id === b.id
    }
    return null;
  };

  atualizarConfig(){
    setTimeout(() => {
      this.ordenacaoSelecionada = this.preferencias.ordenacao;
    }, 300);
  }

  ordernarPartidas(){
     
    this.preferencias.ordenacao = this.ordenacaoSelecionada;
    this.salvarPref(this.preferencias);

    if(this.ordenacaoSelecionada == null ){
      return; 
    }
    if(this.ordenacaoSelecionada.id == 0){
      this.partidas.sort((a, b) => {
         
        let tempoA = a.tempo;
        let tempoB = b.tempo;
        if(a.tempo.length == 1){
         tempoA = "00:0" + a.tempo;
        }else if(a.tempo.length == 2){
         tempoA = "00:" + a.tempo;
        }
        if(b.tempo.length == 1){
         tempoB = "00:0" + b.tempo;
        }else if(b.tempo.length == 2){
         tempoB = "00:" + b.tempo;
        }
         if(tempoA =="INT"){tempoA = "00:45"}
         if(tempoB =="INT"){tempoB = "00:45"}
          if (tempoA < tempoB)
              return -1

          if (tempoA > tempoB)
              return 1

          return 0;

     });
    }else if(this.ordenacaoSelecionada.id == 3){
 
      this.partidas.sort((a, b) => {
        let golsA = parseInt(a.golsCasa) + parseInt(a.golsVisitante);
        let golsB = parseInt(b.golsCasa) + parseInt(b.golsVisitante);
        if(golsA + golsB == 0){
          let tempoA = isNaN(a.tempo) || a.tempo == undefined ? 0:parseInt(a.tempo);
          let tempoB = isNaN(b.tempo) || b.tempo == undefined ? 0:parseInt(b.tempo);
          if(tempoA > tempoB){
            return -1;
          }
        }else{
          return golsA - golsB;
        }
     });
   
    }else if(this.ordenacaoSelecionada.id == 4){
       this.partidas.sort((a, b) => { 
        if(a.media ==-1) {  
          return 0;
        }
        if(parseFloat(a.media) == parseFloat(b.media)){
          return parseInt(a.tempo) - parseInt(b.tempo);
        }
        return (parseFloat(a.media) - parseFloat(b.media)); 
      });
    }else if(this.ordenacaoSelecionada.id == 5){
       this.partidas.sort((a, b) => {
        if(a.media ==-1) {
          return 0;
        }
        if(parseFloat(a.media) == parseFloat(b.media)){
          return parseInt(a.tempo) - parseInt(b.tempo);
        }
        return (parseFloat(b.media) - parseFloat(a.media)); 
        
      });
    } 
  } 

  onCheckTelegramMercado = (goalsAmount) => {
    this.opcaoMercado.forEach(option => {
      let optionGoals = parseFloat(option.title);
      option.show = (goalsAmount > optionGoals) ? false : true;
    });
  }

  async abrirEnvioTelegram(p){
    this.partidaSelecionada = p;
    this.avisarTipCockpit = false;
    this.gruposTelegramSelecionado = [];
    this.opcaoTelegramSelecionadaUnder = null;
    this.opcaoTelegramSelecionadaOver = null;
    this.opcaoTelegramSelecionadaOverHT = null;
    this.opcaoTelegramSelecionadaUnderHT = null;
    if(p.tempo != "FIM"){
      if(parseInt(p.tempo) >46 || p.tempo == null || p.tempo == 'INT'){
        this.mostrarPrimeiroTempo = false;  
      }else{
        this.mostrarPrimeiroTempo = true;
      }
      //let retorno = await this.service.abrirTips(); 
      //if(retorno.status.code == 1){
      //    this.partidaTelegram = p;
      //    this.opcoesTelegram = retorno.value.bots;
       //   this.listaUsuarioCopyTip = retorno.value.logins;
          this.onCheckTelegramMercado((parseInt(p.golsCasa) + parseInt(p.golsVisitante)));
          let modalTelegram = new bootstrap.Modal(document.getElementById('modal-telegram'), {});
          modalTelegram.show();
      //}
    }
  }

  checkTipTotalGol(v){
    if(this.partidaSelecionada != null && this.partidaSelecionada != undefined){
      let totalGol = this.partidaSelecionada.golsCasa + this.partidaSelecionada.golsVisitante;
      console.log("Gols", totalGol); 
    }
    return true;
  }

  clickSinalOverHT(){
    this.opcaoTelegramSelecionadaUnderHT = null;
    return true;
  }

  clickSinalUnderHT(){
    this.opcaoTelegramSelecionadaOverHT = null;
    return true;
  }

  clickSinalOver(){
    this.opcaoTelegramSelecionadaUnder = null;
    return true;
  }

  clickSinalUnder(){
    this.opcaoTelegramSelecionadaOver = null;
    return true;
  }

  selecionarCopyTips(){
    this.listaUsuarioCopyTip.forEach(x=>{
      x.selecionado = this.selecionarContasCopy;
    })
  }

  confirmarEnvioTelegram(){
   /* if(this.gruposTelegramSelecionado.length == 0 && this.avisarTipCockpit == false){
      Swal.fire({
        title: 'Envio de sinal',
        'text':"Selecione um grupo de Telegram ou Cockpit para enviar o sinal.", 
        confirmButtonText: 'Fechar',
      })
      return; 
    }*/
    
    if(this.opcaoTelegramSelecionadaOver == null && 
      this.opcaoTelegramSelecionadaUnder == null && 
      this.opcaoTelegramSelecionadaOverHT == null && 
      this.opcaoTelegramSelecionadaUnderHT == null){
      Swal.fire({
        title: 'Seleção de mercado',
        'text':"Informe o mercado para enviar o sinal", 
        confirmButtonText: 'Fechar',
      })
    }else{
   
      let mensagem = 'Deseja realmente enviar o sinal da partida ' + this.partidaTelegram + '?';
      Swal.fire({
        title: 'Envio de sinal',
        'text':mensagem, 
        showDenyButton: true,
        confirmButtonText: 'Enviar',
        denyButtonText: `Desistir`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.enviarSinal();
        }  
      });
    } 
  }
 
  async enviarSinal(){
    let opcao = "";

    if(this.opcaoTelegramSelecionadaOver != null){ opcao = this.opcaoTelegramSelecionadaOver; }
    if(this.opcaoTelegramSelecionadaUnder != null){ opcao = this.opcaoTelegramSelecionadaUnder; }
    if(this.opcaoTelegramSelecionadaOverHT != null){ opcao = this.opcaoTelegramSelecionadaOverHT; }
    if(this.opcaoTelegramSelecionadaUnderHT != null){ opcao = this.opcaoTelegramSelecionadaUnderHT; }
    
    let dadosEnvio ={"partida":this.partidaSelecionada, 
                     "bots":null,
                     "avisarTipCockpit":this.avisarTipCockpit,
                     "idMercado":opcao.idMercado,
                     "labelMercado":opcao.label,
                     "operador":opcao.operador,
                     "logins":[],
                     "porcentagem":this.porcentagemCopyTip};

    $('#modal-telegram').unblock(); 
    $('#modal-telegram').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 

    let retorno = await this.service.enviarSinal(dadosEnvio);
    $('#modal-telegram').unblock(); 
 
    if(retorno.status.code ==1){
      Swal.fire({
        title: 'Envio de sinal',
        'text':'Sinal enviado com sucesso.', 
        confirmButtonText: 'Fechar',
      });
      $('#btnFecharModalTelegram').click();
    }else{
      Swal.fire({
        title: 'Envio de sinal',
        'text':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
  }

  subscribeCockpitSocket(){ 
    this.socketCockpit.init();
    this.subscriberServico = this.ea.subscribe('HANDLE_CP_MESSAGE', (message)=>{
      this.processarCockpit(message);
    });
    this.subscriberServico = this.ea.subscribe('EVT_IN_PLAY', (value)=>{
      this.processarInPlay(value);
   });
  }

  cssEntrouPartida(p){
    if(p.entrouPartida == true){
      p.cssEntrada="scoreboard entrou-partida"; 
    }else{
      p.cssEntrada= "scoreboard";
    }
  }

  processarInPlay(value){
    if(value != null){
      if(this.partidas){
        value.forEach(inp=>{
          this.partidas.forEach(p=>{
              if(p.id == inp.idPartida){
                p.entrouPartida=true;
              }
          });
        });
      }
    }
  } 

  getCssColunaGol(valor){
    if(valor != ""){
      if(parseInt(valor) >=3){
              return "info-add potencial-over";
      } 
    }
    return "info-add"; 
  }

  convert(today, match){
    
    if(match.tempo != 'In-Play'){
      today.golsCasa =  match.golsCasa;
      today.golsVisitante =  match.golsVisitante;
    }
    
    today.casa =  match.casa;
    today.acrescimo =  match.acrescimo;  
    today.link =  match.link;
    if(match.tempo != undefined && match.tempo != "In-Play"){
      match.tempo = match.tempo.replaceAll("Today", "");
      match.tempo = match.tempo.replaceAll("END", "FIM");
      match.tempo = match.tempo.replaceAll("Starting soon", "Em breve");
      match.tempo = match.tempo.replaceAll("HT", "INT"); 
      match.tempo = match.tempo.replaceAll("Starting in", "Em ");
      today.tempo =  match.tempo; 
    } 
  }

  confirmarEncerrarPartida(p){
    this.partidaSelecionada = p;
    Swal.fire({
      title: 'ENCERRAR PARTIDA',
      'text':"Deseja realmente encerrar essa partida?", 
      showDenyButton: true,
      confirmButtonText: 'Sim',
      denyButtonText: `Desistir`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.encerrarPartida();
      }  
    });
  }

  async encerrarPartida(){
    await this.service.encerrarPartida({idPartida:this.partidaSelecionada.id});
    Swal.fire({
      title: 'Encerrar partida',
      'text':"Partida encerrada com sucesso", 
      confirmButtonText: 'Fechar',
    })
  }

  confirmarRemoverPartida(p){
    this.partidaSelecionada = p;
    Swal.fire({
      title: 'Deseja realmente remover essa partida?',
      'text':"REMOVER PARTIDA", 
      showDenyButton: true,
      confirmButtonText: 'Enviar',
      denyButtonText: `Desistir`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.removerPartida();
      }  
    });
  }

  async removerPartida(){
      await this.service.removerPartida({idPartida:this.partidaSelecionada.id});
      Swal.fire({
        title: 'Remover partida',
        'text':"Partida removida com sucesso", 
        confirmButtonText: 'Fechar',
      })
  }



  async doCopy(){
    this.mensagem = "Selecione o mercado desejado.";
    if(this.idMercado =="" || this.idMercado == null){
      this.mensagem = "Selecione o mercado desejado.";
      return ;
    }
    
    let selecionado = this.contasCopy.filter(i => i.selecionado == true);
    
    if(selecionado == 0){ 
      this.mensagem = "Selecione as contas que deseja operar.";
      return;
    }
    let validarPercentual = true;
    this.contasCopy.every(c=>{
      if(c.selecionado == true && (c.porcentagem == '' || c.porcentagem == null || c.porcentagem == 0 )){
        this.mensagem = "Informe o percentual da conta " + c.nome;        
        validarPercentual = false;
       return false;
      }
    })

    if(!validarPercentual){
      return;
    }

    $('#body-copy-odds').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
 
    let retorno = await this.service.doCopy({
        "idPartidaBetFair":this.partida.idPartidaBetFair,
        "idMercado":this.idMercado,
        "operador":this.operador,
        "login":this.contasCopy,
        "odd":this.odd,
        "percentualEntrada":0,
        "mercado":this.mercado
    });  
    if(retorno.status.code == 1){
      Swal.fire({
        title: 'Entrada realizada',
        'text':"Sua entrada foi realizada com sucesso.", 
        confirmButtonText: 'Fechar',
      });
      $('#btn-odds-close-copy').click();
    }else{
      Swal.fire({
        title: 'Atenção',
        'text':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
    this.fecharBackDrop();
    this.mensagem = retorno.status.message;
    this.selecao = "";
    $('#body-copy-odds').unblock(); 
  }

  fecharBackDrop(){
    setTimeout(() => {
      $("div.modal-backdrop.fade.show").remove(); 
    }, 100);
  }

  fecharCopy(){
    this.fecharBackDrop();
  }

  async doCopyAll(){
      this.mensagem = "Selecione o mercado desejado.";
      if(this.idMercado =="" || this.idMercado == null){
        this.mensagem = "Selecione o mercado desejado.";
        return ;
      } 
      if(this.user.percEntrada =="" || this.user.percEntrada == null || this.user.percEntrada == 0){
        this.mensagem = "Informe a % de entrada.";
        return ;
      } 

      $('#body-copy-odds').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      }); 
   
      let retorno = await this.service.doCopyAll({
          "idPartidaBetFair":this.partida.idPartidaBetFair,
          "idMercado":this.idMercado,
          "operador":this.operador,
          "login":[],
          "odd":this.odd,
          "percentualEntrada":this.user.percEntrada,
          "mercado":this.mercado
      });  
  
      this.mensagem = retorno.status.message;
      this.selecao = "";
      $('#body-copy-odds').unblock(); 
      this.fecharBackDrop();
  }

  processarNovoDia(data){
    this.pesquisarFuturo(data,false); 
  }

  setarValorEntradaCockpit(){
    this.valorEntrada = "R$ " + parseFloat((this.saldoConta * this.user.percEntrada)/100).toFixed(2);
    this.lucroEntrada =  + parseFloat(parseFloat(this.odd) * parseFloat((this.saldoConta * this.user.percEntrada)/100).toFixed(2)).toFixed(2);
    if(isNaN(this.lucroEntrada)){
      this.lucroEntrada = "R$ 0,00";
    }else{
      this.lucroEntrada =  "R$ " + this.lucroEntrada;
    }
  }

  setarValorEntrada(){
    this.contasCopy.forEach(c=>{
      c.porcentagem = this.user.percEntrada;
      this.onSetCurrencyValue(c);
    })
  }

  async pesquisarFuturo(data, silent){
    if(!silent){
      $('#tabPartidas').unblock(); 
      $('#tabPartidas').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      }); 
    }
    let retorno = await this.service.searchPanelFuturo(data);
    //console.log("retorno.: ", retorno);
    if(!silent){
      $('#tabPartidas').unblock(); 
    }
    if(retorno.status.code ==1){
      this.partidas = retorno.value.itens; 
      this.fullToday = retorno.value.itens;
      this.medias = retorno.value.medias
    } 
  }

  async buscarOportunidadeOver15(){
    // let selecionados = this.partidas.filter(i => {return i.tempo == 'INT' && ((parseInt(i.golsCasa) + parseInt(i.golsVisitante)) ==1) });
    // if(selecionados.length > 0){    
    //   let dados = await this.service.buscarOportunidadeOver15(selecionados);
    //   this.partidas.forEach(p=>{
    //     dados.value.forEach(d=>{
    //       if(p.idPartidaBetFair == d.idPartidaBetFair){
    //         p.potencialOver15 = true;  
    //       }
    //     });
    //   });
    // } 
  }  

  iniciarlizarTimeoutOver(){
    setInterval(() => {
      this.buscarOportunidadeOver15();
    }, 3000);
  }

  async buscarContasCopy(){
    let retorno = await this.service.buscarContasCopy();
    if(retorno.status.code == 1){
      this.contasCopy = retorno.value;
      this.contasCopy.forEach(c=>{
        c.porcentagem = 1;
      })
    }
  }

  async onChooseOdds(idMercado, operador, event){
    this.mensagemEntrada="";
    this.lucroEntrada="R$ 0,00";
    this.odd = "";
    this.onSelectButton(event);
    this.operador = operador;
    this.idMercado = idMercado;
    this.mercado = (operador == '+'? "MAIS DE " + this.idMercado : "MENOS DE " + this.idMercado) +" GOLS";
    this.selecao = "VOCÊ SELECIONOU " + this.mercado;
    this.mensagem = ""; 
    if(this.isUserInRole('MASTER') || this.isUserInRole('SUPER')){
      $('#body-copy-odds').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>"   
      }); 
    }else{
      $('#container-entrada').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      }); 
    } 
 
    let retorno  =  await this.service.buscarOdd(this.idMercado, this.operador =='+'?'OVER':'UNDER', this.partida.idPartidaBetFair);
    if(retorno.status.code == 1){
      this.odd = retorno.value.valor;
    }else{
      Swal.fire({
        title: 'Atenção',
        'text':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
    if(this.isUserInRole('MASTER') || this.isUserInRole('SUPER')){
      $('#body-copy-odds').unblock(); 
    }else{
      $('#container-entrada').unblock(); 
    }
    this.setarValorEntradaCockpit();
  }

  onSelectButton = (e) => { 
    const elems = [...document.querySelectorAll('.odds-odds')]
    const elem = e.target
    elems.forEach(element => {
      element.classList.remove('selected')
    });  
    elem.classList.toggle('selected')
  }

  async onOpenOdds(p){
    this.user.percEntrada = "1";
    this.selectAll=false;
    this.mensagemEntrada = "";
    this.lucroEntrada = "R$ 0,00";
    this.selecao = "Selecione o mercado ao lado";

    let retorno = await this.service.buscarSaldoConta();
    if(retorno.status.code == 1){
      this.saldoConta = retorno.value;
    }

    if(this.isUserInRole('OPERADOR') || this.isUserInRole('MASTER') || this.isUserInRole('SUPER')){
      await this.buscarContasCopy(); 
    }
    this.partida = p.partida;
    this.partida.golsCasa = p.golsCasa;
    this.partida.golsVisitante = p.golsVisitante;
    this.operador = null;
    this.idMercado = null;
    this.odd="",
    this.porcentagem="",
    this.mensagem = "Selecione o mercado desejado.";
    let modalAddOdds = new bootstrap.Modal(document.getElementById('modal-example'), {});
    modalAddOdds.show();
    this.onCheckOpenedOdds((parseInt(p.golsCasa) + parseInt(p.golsVisitante)));
  }
  
  onCheckOpenedOdds = (goalsAmount) => {
    this.oddsOptions.forEach(option => {
      let optionGoals = parseFloat(option.title);
      option.show = (goalsAmount > optionGoals) ? false : true;
    });
  }

  getTempo(t){
    if(t == "END") {return "FIM"}
    if(t == null) {return "-"}
    return t ;
  }

  async ativarConta(p, value){
    await this.service.ativarConta({'idConta':p.login, 'situacao':value});
  }

  intervalRemoverFinalizadas(){
    this.intervalAtualizacao = setInterval(() => {
      
      this.partidas = this.partidas.filter(i => i.tempo != 'FIM');
      this.fullToday = this.fullToday.filter(i => i.tempo != 'FIM');
      this.ordernarPartidas();
    }, 60000);
  } 

  startLiveUpdate(){  
    this.subscriberServico = this.ea.subscribe('HANDLE_PAINEL_STATUS', (value)=>{
      this.processarMatches(value);
    });
    this.subscriberDia = this.ea.subscribe('EVT_TROCAR_DIA', (value)=>{
      this.processarNovoDia(value);
    });
  }      
  
  detached(){
    this.socketCockpit.desligar();
    if(this.subscriberServico){
      this.subscriberServico.dispose();
    }
    if(this.subscriberDia){
      this.subscriberDia.dispose();
    }
    if(this.intervalAtualizacao){
      clearInterval(this.intervalAtualizacao);
    }
    if(this.intervalDados){
      clearInterval(this.intervalDados);
    }
  }
 
  processarMatches(e){
    let dados = JSON.parse(e.value);
    this.ultimaAtualizacao = dados.horario;
    if(this.partidas && this.partidas != null){
      
      dados.matches.forEach(match => {  
          
          this.partidas.forEach(today => {
            this.checarVisibilidateFiltro(today);
            this.cssEntrouPartida(today);
            if(today.casa.toLowerCase() == match.casa.toLowerCase() && today.visitante.toLowerCase() == match.visitante.toLowerCase()){
              this.convert(today,match);
            } 
          }); 
          this.fullToday.forEach(today => {
            if(today.casa.toLowerCase() == match.casa.toLowerCase() && today.visitante.toLowerCase() == match.visitante.toLowerCase()){
              this.convert(today,match);
            } 
        });
      });
      this.ordernarPartidas();
    }
  }

  processarCockpit(message){
    let mensagem = JSON.parse(message.value);
    if(mensagem.tipo == "LANCE_CERTO_OVER"){
      document.getElementById('player-over').play();

      let tipo = "";
      if(mensagem.value.tipo == 1){
        tipo = "danger";
      }else if(mensagem.value.tipo == 2){
        tipo = "warning";
      }else if(mensagem.value.tipo == 3){
        tipo = "info";
      }
      let tip = "";
      if(mensagem.value.login != null){
        tip+="<div style='display: flex;flex-direction: column;'>"
        tip+="	<div style='color: white;font-weight: 600;'><i style='color:red' class='fa-solid fa-bell'></i> Alerta de OVER !  </div>"
        tip+="	<div style='display: flex;flex-direction: row;' >"
        tip+="		<div style='flex-basis: 5em;'>"
        tip+="			<img style='width:62%;background-color: #fff;padding: 1px;border-radius: 6px;' src='#"+ mensagem.value.login.id + ".png'/>"
        tip+="		</div>"
        tip+="		<div style='display: flex;flex-direction: column;' >"
        tip+="		<div style='font-weight: bold;font-size: 17px;'>"
        tip+=mensagem.value.casa + ' vs ' + mensagem.value.visitante
        tip+="		</div>"
        tip+="		<div style='font-size: 15px;color: white;font-weight: 400;'>"
        tip+="- " + mensagem.value.login.tituloAlertaOver + " -"
        tip+="		</div>"
        tip+="	</div>"
        tip+="	</div>" 
        tip+="</div>"
      }else{
        tipo = "danger";
        tip+="<div style='display: flex;flex-direction: column;'>"
        tip+="	<div style='color: white;font-weight: bold;'>Alerta de OVER !</div>"
        tip+="	<div style='display: flex;flex-direction: row;' >"
        tip+="		<div style='display: flex;flex-direction: column;' >"
        tip+="		<div style='font-weight: bold;font-size: 19px;'>"
        tip+=mensagem.value.casa + ' vs ' + mensagem.value.visitante
        tip+="		</div>"
        tip+="		<div style='font-size: 16px;color: white;font-weight: 400;'>"
        tip+="- Cockpit -"
        tip+="		</div>"
        tip+="	</div>"
        tip+="	</div>"
        tip+="</div>"
      }
 
      SnackBar({
        message:tip,
        width: "350px",
        status: tipo,
        timeout: 0 
      });

    }else if (mensagem.tipo == "ESTATISTICA_PARTIDA"){
     if(this.swalInicial){
       this.swalInicial.close();
     }
      $('#table-body').unblock(); 
      if(this.partidas && this.partidas != null){
        let dados = mensagem.value.itens; 
        this.partidas.forEach(p=>{
          this.cssEntrouPartida(p);
          this.checarVisibilidateFiltro(p);
          dados.forEach(score =>{
            if(p.partida.id == score.idPartida ){
              p.scoreBing.plus = score.plus;
              p.scoreBing.rd = score.rd;
              p.scoreBing.sd = score.sd;
              p.scoreBing.fire = score.fire;
              p.linkBet365 = score.linkBet365;
              p.link = score.link;
              p.scoreBing.cartaoAmareloCasa = score.cartaoAmareloCasa;
              p.scoreBing.cartaoVermelhoCasa = score.cartaoVermelhoCasa;
              p.scoreBing.cartaoAmareloVisitante = score.cartaoAmareloVisitante;
              p.scoreBing.cartaoVermelhoVisitante = score.cartaoVermelhoVisitante;
              p.acrescimo = score.acrescimo;
              p.scoreBing.indicePressaoCasa = score.indicePressaoCasa;
              p.scoreBing.indicePressaoVisitante = score.indicePressaoVisitante;

              if(p.partida.media == -1){
                p.media = score.media;
              }
              if(score.rd){
                p.golsVisitante = score.rd.gg
                p.golsCasa = score.rd.hg
              }
              let tempo = score.status;
              tempo = tempo.replaceAll("HT","INT");
              tempo = tempo.replaceAll("END","FIM");
              tempo = tempo.replaceAll("FT","FIM");
              tempo = tempo.replaceAll("NS","Em breve");
              p.tempo = tempo;
            }
          });
        });
        this.ordernarPartidas();
      }
    } 
  }

  async atualizar(){
    let retorno = await this.service.processPanel();
    if(retorno.status.code ==1){
      this.partidas = retorno.value.itens;
      this.fullToday = retorno.value.itens; 
      this.medias = retorno.value.medias
    } 
    this.pesquisar(this.today,false);
  }
  
  abrirPartida(p){
    if(p.link && p.link.length > 0){
     window.open(p.link,'_blank');
    }
  }

  abrirBet365(item){
    if(item.linkBet365 != "" && item.linkBet365 != null){ 
      window.open(item.linkBet365, '_blank');
    }
  }

  abrirLinkMedia(item){
    if(item.linkMedia != "" && item.linkMedia != null){ 
      window.open(item.linkMedia, '_blank');
    }
  }

  ajustarLink(p){     
    if(p.link != null){    
      return "head-grid head-partida clicavel";  
    }else{   
      return "head-grid head-partida "; 
    }
  } 

  abrirPesquisa(item){  
    if(item.link != "" && item.link != null){ 
      window.open(item.link, '_blank');
    }
  }

  doSearch(e){  
    
    this.partidas = [];    
    if(this.time == "" || this.time == undefined){   
      this.partidas = this.fullToday; 
    }else{
      let palavras = this.time.split(" ");

      const filtrarObjetos = (fullToday, palavras) => {
        const palavrasValidas = palavras.filter(palavra => palavra.trim() !== '');
      
        return fullToday.filter(obj => {
          return palavrasValidas.some(palavra =>
            obj.casa.nome.toLowerCase().includes(palavra) || obj.visitante.nome.toLowerCase().includes(palavra)
          );
        });
      };

      this.partidas = filtrarObjetos(this.fullToday, palavras);

      /*params.forEach(v=>{
        if(v != ""){
          this.partidas = this.fullToday.filter(i => i.casa.nome.toLowerCase().indexOf(v.toLowerCase()) != -1 || 
                                                   i.visitante.nome.toLowerCase().indexOf(v.toLowerCase()) != -1  );
        }
      }); */
    }
    return true;
  }

  getColor(index){
    if(index % 2 == 0){
      return "odd-painel";  
    }else{
      return "even-painel";
    }
  }
  
  async pesquisarSilent(data, silent){
    if(!silent){
      $('#tabPartidas').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      }); 
    }
    let retorno = await this.service.searchPanel(data);
        
    //console.log("retorno.: ", retorno); 
    if(!silent){
      $('#tabPartidas').unblock(); 
    }
    if(retorno.status.code ==1){
      this.partidas = retorno.value;
      if(this.partidas){
        this.partidas.forEach(p=>{
          p.visivel = true;
         // p.confrontos = JSON.parse(p.confrontos);
         // p.overs = JSON.parse(p.overs);
        })
      } 
      this.fullToday = retorno.value;
      this.listaGols = [];

      let golsInicialList = [...new Set(retorno.value.map(item => item.golsInicial))];
      this.listaGols= golsInicialList.sort((a, b) => a - b);
      this.listaGols.unshift("Todos");

      this.aplicarFiltroOver();
      this.ordernarPartidas();
      this.doSearch();
    } 
  }

  async pesquisar(data, silent){
    if(!silent){
      $('#tabPartidas').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      }); 
    }
    let retorno = await this.service.searchPanel(data);
        
    this.swalInicial = Swal.fire({ 
      title: 'Carregando os dados...', 
      html:"Aguarde enquanto carregamos os dados das partidas. <br><div class='window'><div class='nav'><div class='statusBar'></div></div></div>", 
      showConfirmButton: false
    });

    //console.log("retorno.: ", retorno); 
    if(!silent){
      $('#tabPartidas').unblock(); 
    }
    if(retorno.status.code ==1){
      this.partidas = retorno.value;
      if(this.partidas){
        this.partidas.forEach(p=>{
          p.visivel = true;
         // p.confrontos = JSON.parse(p.confrontos);
         // p.overs = JSON.parse(p.overs);
        })
      } 
      this.fullToday = retorno.value;
      this.medias = retorno.value.medias;
      this.swalInicial.close();
      this.fullToday = retorno.value;
      
      this.listaGols = [];

      let golsInicialList = [...new Set(retorno.value.map(item => item.golsInicial))];
      this.listaGols= golsInicialList.sort((a, b) => a - b);
      this.listaGols.unshift("Todos");
      this.ordernarPartidas();
    } 
  }
  
  onSelectTab = (clickedTab) => {
    switch (clickedTab) {
      case 1:
        this.tabs.tab1 = true;
        this.tabs.tab2 = false;
        this.tabs.tab3 = false;
        this.tabs.tab4 = false;
        this.tabs.tab5= false;
        break;
      case 2:
        this.tabs.tab1 = false;
        this.tabs.tab2 = true;
        this.tabs.tab3 = false;
        this.tabs.tab4 = false;
        this.tabs.tab5= false;
        break;
      case 3:
        this.tabs.tab1 = false;
        this.tabs.tab2 = false;
        this.tabs.tab3 = true;
        this.tabs.tab4 = false;
        this.tabs.tab5= false;
        break;
      case 4:
        this.tabs.tab1 = false;
        this.tabs.tab2 = false;
        this.tabs.tab3 = false;
        this.tabs.tab4 = true;
        this.tabs.tab5= false;
        break;
      case 5:
          this.tabs.tab1 = false;
          this.tabs.tab2 = false;
          this.tabs.tab3 = false;
          this.tabs.tab4 = false;
          this.tabs.tab5 = true;
          break;
      default:
        break;
    }
  }

  onEditData = () => {
    this.onEditing = !this.onEditing
    if(!this.onEditing){
      this.atualizarDadosPartida(this.partidas);
    }
  };

  async atualizarDadosPartida(partidas){

    let partidasAlteradas = [];
    partidas.forEach(e=>{
      if(e.idScoreBingCasaOriginal != e.idScoreBingCasa){
        e.idScoreBingCasaOriginal = e.idScoreBingCasa;
        partidasAlteradas.push(e);
      }
      if(e.idScoreBingVisitanteOriginal != e.idScoreBingVisitante){
        partidasAlteradas.push(e);
        e.idScoreBingVisitanteOriginal = e.idScoreBingVisitante;
      }
      if(e.nomeCasaOriginal != e.casa){
        partidasAlteradas.push(e);
      }
      if(e.nomeVisitanteOriginal != e.visitante){
        partidasAlteradas.push(e);
      } 
      if(e.pais != e.nomePais){
        partidasAlteradas.push(e); 
      }
      if(e.liga != e.nomeLiga){
        partidasAlteradas.push(e);
      } 
    });

    if(partidasAlteradas.length > 0){
      await this.service.atualizarDadosPartida(partidasAlteradas);
    }
  }

  uniqueBy(arr, prop){
    return arr.reduce((a, d) => {
      if (!a.includes(d[prop])) { a.push(d[prop]); }
      return a;
    }, []);
  }

  onHighLightRow = (p) => this.selectedRow = (this.selectedRow === p.id) ? null : p.id;

  onSetCurrencyValue = (p) => {
    const saldo = p.saldo;
    const porcentagem = p.porcentagem;
    const result = parseFloat((saldo * porcentagem)/100).toFixed(2);
    p.percVal = result;
  }
 
  onSetProgressVolume = (type, status, obj) => {
    const floor = Math.floor
 
    if (!status) return 0;
    if (obj == 'plus') {
      let { chuteGolCasa, chuteGolVisitante, chuteForaCasa, chuteForaVisitante, 
            ataquePerigosoCasa, ataquePerigosoVisitante, ataqueCasa, ataqueVisitante, posseCasa, posseVisitante } = status;
      chuteGolCasa = floor(chuteGolCasa)
      chuteGolVisitante = floor(chuteGolVisitante)
      chuteForaCasa = floor(chuteForaCasa)
      chuteForaVisitante = floor(chuteForaVisitante)
      ataquePerigosoCasa = floor(ataquePerigosoCasa)
      ataquePerigosoVisitante = floor(ataquePerigosoVisitante)
      ataqueCasa = floor(ataqueCasa)
      ataqueVisitante = floor(ataqueVisitante)
      posseCasa = floor(posseCasa) 
      posseVisitante = floor(posseVisitante)
      switch (type) {
        case 'nogol':
          return this.ruleOfThree(chuteGolCasa, chuteGolVisitante)
        case 'pfora':
          return this.ruleOfThree(chuteForaCasa, chuteForaVisitante)
        case 'ataquep':
          return this.ruleOfThree(ataquePerigosoCasa, ataquePerigosoVisitante)
        case 'ataque':
          return this.ruleOfThree(ataqueCasa, ataqueVisitante)
        case 'posse':
          return this.ruleOfThree(posseCasa, posseVisitante)
        default:
          return 0;
      }
    } else {
      let { escanteioCasa, escanteioVisitante } = status;
      escanteioCasa = floor(escanteioCasa)
      escanteioVisitante = floor(escanteioVisitante)
      switch (type) {
        case 'esc':
          return this.ruleOfThree(escanteioCasa, escanteioVisitante)
        default:
          escanteioCasa
      }
    }
    return 0;
  }

  itMustBeShown = (type, status, obj) => {
    const floor = Math.floor
    if (!status) return false;
    if (obj === 'plus') {
      let { chuteGolCasa, chuteGolVisitante, chuteForaCasa, chuteForaVisitante, 
           ataquePerigosoCasa, ataquePerigosoVisitante, ataqueCasa, ataqueVisitante, 
           posseCasa, posseVisitante } = status;
      chuteGolCasa = floor(chuteGolCasa)
      chuteGolVisitante = floor(chuteGolVisitante)
      chuteForaCasa = floor(chuteForaCasa)
      chuteForaVisitante = floor(chuteForaVisitante)
      ataquePerigosoCasa = floor(ataquePerigosoCasa)
      ataquePerigosoVisitante = floor(ataquePerigosoVisitante)
      ataqueCasa = floor(ataqueCasa)
      ataqueVisitante = floor(ataqueVisitante)
      posseCasa = floor(posseCasa)
      posseVisitante = floor(posseVisitante)
      switch (type) {
        case 'nogol':
          return (chuteGolCasa+chuteGolVisitante>0) ? true : false;
        case 'pfora':
          return (chuteForaCasa+chuteForaVisitante>0) ? true : false;
        case 'ataquep':
          return (ataquePerigosoCasa+ataquePerigosoVisitante>0) ? true : false;
        case 'ataque':
          return (ataqueCasa+ataqueVisitante>0) ? true : false;
        case 'posse':
          return (posseCasa+posseVisitante>0) ? true : false;
        default:
          return false;
      }
    } else {
      let { escanteioCasa, escanteioVisitante } = status;
      escanteioCasa = floor(escanteioCasa)
      escanteioVisitante = floor(escanteioVisitante)
      switch (type) {
        case 'esc':
          return (escanteioCasa+escanteioVisitante>0) ? true : false;
        default:
          return false;
      }
    }
  }

  ruleOfThree = (home, visit) => {
    const total = home + visit
    return (total > 0) ? (Math.floor((home * 100) / total)) : 0
  }

  onToggleCheckAccounts = () => {
    this.contasCopy.forEach(conta => {
      conta.selecionado = this.selectAll;
    });
  }
}       
