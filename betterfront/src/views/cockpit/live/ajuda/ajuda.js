import { BasicView } from 'views/BasicView';

export class Ajuda extends BasicView {


  constructor(...rest){
    super(...rest); 
  } 

  getNome(){
    return this.getProperty('name').split(' ')[0];
  }
}       
