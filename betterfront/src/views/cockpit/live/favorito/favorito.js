import { BasicView } from 'views/BasicView';
import Swal from 'sweetalert2';

export class Favorito extends BasicView {

  nomeTime;
  listaFavoritoUnder;
  listaFavoritoOver;
  listaTimes;
  favoritoSelecionado;
  layerContainer;
  pesquisaOver;
  pesquisaUnder;
  constructor(...rest){
    super(...rest); 
  } 

  attached(){

    this.buscarFavoritos();
  }

  confirmarRemoverFavorito(fav, layer){
    this.favoritoSelecionado = fav;
    this.layerContainer = layer;
    Swal.fire({
      title: 'Remover favoritos',
      'text':"Deseja realmente remover esse favorito?", 
      showDenyButton: true,
      confirmButtonText: 'Sim, remover',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.removerFavorito();
      }  
    });
  }
  
  async removerFavorito(){
    this.block(this.layerContainer); 
    let retorno = await this.service.removerFavorito(this.favoritoSelecionado.id);
    this.unblock(this.layerContainer); 

    if(retorno.status.code == 1){
      Swal.fire({
        title: 'Remover favoritos',
        'text': "Favorito removido com sucesso", 
        confirmButtonText: 'Fechar',
        allowEscapeKey : true
      });
      this.buscarFavoritos();
    }else{
      Swal.fire({
        title: 'Remover favoritos',
        'text': retorno.status.message,
        confirmButtonText: 'Fechar',
        allowEscapeKey : true
      });
    }
  }

  async salvarFavoritos(){
    let lista = [];
    this.listaTimes.forEach(t =>{
      if(t.over || t.under ){ 
        lista.push(t);
      }
    });

    if(lista.length > 0){
    
      this.unblock('modal-pesquisa'); 
      let retorno = await this.service.favoritar(lista);
      this.unblock('modal-pesquisa'); 

      if(retorno.status.code == 1){
        Swal.fire({
          title: 'Favoritos',
          'text': "Favorito salvo com sucesso", 
          confirmButtonText: 'Fechar',
          allowEscapeKey : true
        });
        this.buscarFavoritos();
      }else{
        Swal.fire({
          title: 'Favoritos',
          'text': retorno.status.message,
          confirmButtonText: 'Fechar',
          allowEscapeKey : true
        });
      }
    }
  }

  pesquisarOver(){
    this.listaFavoritoOver.forEach(o=>{
      if(o.nome.toLowerCase().indexOf(this.pesquisaOver.toLowerCase()) !=-1){
        o.visivel = true;
      }else{
        o.visivel = false;
      }
    })
  }

  pesquisarUnder(){
    this.listaFavoritoUnder.forEach(o=>{
      if(o.nome.toLowerCase().indexOf(this.pesquisaUnder.toLowerCase()) !=-1){
        o.visivel = true;
      }else{
        o.visivel = false;
      }
    })
  }

  async buscarFavoritos(){
    this.listaFavoritoOver = [];
    this.listaFavoritoUnder = [];
    this.unblock('container-favorito'); 
    let retorno = await this.service.buscarFavorito();  
    this.unblock('container-favorito'); 
    if(retorno.status.code == 1){
       retorno.value.forEach(f => {
          f.visivel = true;
          
          if(f.over){
            this.listaFavoritoOver.push(f);
          }
          if(f.under){
            this.listaFavoritoUnder.push(f);
          }
       });
       setTimeout(() => {
         this.listaFavoritoOver = this.listaFavoritoOver.sort((a, b) => a.nome.localeCompare(b.nome));
         this.listaFavoritoUnder = this.listaFavoritoUnder.sort((a, b) =>  a.nome.localeCompare(b.nome));
       }, 300);
    }else{
      Swal.fire({
        title: 'Favoritos',
        'text':retorno.status.message, 
        confirmButtonText: 'Fechar',
        allowEscapeKey : true
      });
    }
  }

  abrirModal(){
    let modal = new bootstrap.Modal(document.getElementById('modal-pesquisa'), {backdrop: 'static', keyboard: false});
    modal.show();
    setTimeout(() => {
      $('#nomeTime').focus();
    }, 900);
  }

  async pesquisar(){
    
    if(this.nomeTime.length < 3){
      Swal.fire({
        title: 'Pesquisa de times',
        'text': "Informe pelo menos três letras do nome do time que deseja pesquisar.", 
        confirmButtonText: 'Fechar',
        allowEscapeKey : true
      });   
      return
    }
    $('#modal-pesquisa').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
    let retorno = await this.service.pesquisarTime(this.nomeTime);  
    $('#modal-pesquisa').unblock(); 

    if(retorno.status.code == 1){
      if(retorno.value.length == 0){
        Swal.fire({
          title: 'Pesquisa de times',
          'html': "Não foi encontrado nenhum time com nome semelhante a <strong>" + this.nomeTime + "</strong>", 
          confirmButtonText: 'Fechar',
          allowEscapeKey : true
        });  
      }else{
        this.listaTimes = retorno.value;
      }
    }else{
      Swal.fire({
        title: 'Pesquisa de times',
        'text':retorno.status.message, 
        confirmButtonText: 'Fechar',
        allowEscapeKey : true
      });
    }
   
  }
}       
