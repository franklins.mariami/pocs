import { BasicView } from 'views/BasicView';
import flatpickr from "flatpickr";
import { Portuguese } from "flatpickr/dist/l10n/pt.js"
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import {Chart} from './../../../../../node_modules/chart.js/dist/Chart.js';

@inject(EventAggregator)
export class LucroDiario extends BasicView {
  lucrosData = [];
  contaSelecionada;
  operacoes;
  interval;
  today; 
  dataSelecionada
  status={};
  chartLabels = [];
  chartValues = [];
  chart = null;
  chartData = {};
  nomeConta="";

  constructor(eventAggregator, ...rest){
    super(...rest); 
    this.ea = eventAggregator;
  }
  
  attached(){
    this.today = this.ajustarDia();
    this.dataSelecionada = this.today;
    this.fillDatePicker();
    this.buscarLucro();
    this.iniciarlizarTimeOut();
    this.onSetCharts();
    if(!this.isUserInRole('MASTER') && !this.isUserInRole('SUPER')) {
      this.buscarStatusGrafico();
      this.buscarInplay();
    }else{
      this.buscarLucro();
    }
  }
 
  doSearch(e){  
    this.contasData = [];   
    if(this.nomeConta == ""){  
      this.lucrosData = this.lucrosDataOriginal; 
    }else{
      this.lucrosData = this.lucrosDataOriginal.filter(i => (i.login.nome.toLowerCase().indexOf(this.nomeConta.toLowerCase()) != -1  || i.login.email.toLowerCase().indexOf(this.nomeConta.toLowerCase()) != -1 )   );
    }
    return true;
  }

  async buscarInplay(){
    const retorno = await this.service.buscarInPlay();
    if(retorno.status.code == 1){
        this.ea.publish('EVT_IN_PLAY', retorno.value.inPlay);
    }
  }

  iniciarlizarTimeOut(){

    if(!this.isUserInRole('MASTER') && !this.isUserInRole('SUPER')) {
      this.intervalSaldo = setInterval(() => {
         this.buscarStatusGrafico();
      }, 180000);
      this.intervalInplay = setInterval(() => {
        this.buscarInplay();
     }, 3000);
    }else{
      this.interval = setInterval(() => {
        let data = this.getInplayDate();
        if(data == null){
          this.buscarLucro();
        }else if (data == this.today){
          this.buscarLucro(); 
        }
       }, 3000);
    }
  } 

  async buscarStatusGrafico(){
    const retorno = await this.service.buscarStatusClienteCockpit();
    if(retorno.status.code == 1){
        this.status = retorno.value;
        this.chartLabels = [];
        this.chartValues = [];
        this.status.entradas.forEach(e=>{
          this.chartLabels.push(e.hora); 
          this.chartValues.push(parseFloat(e.valor));
        });

       let chartData = {
        labels: this.chartLabels,
        datasets: [{
          data: this.chartValues,
          label: 'Lucro diário', 
          backgroundColor: 'transparent',
          borderColor: '#79a3fd82',
          borderWidth: 1,
          pointBackgroundColor: '#28a745'
        }]
      };
      this.chart.data = chartData;
      this.chart.update();
    };
  }

  async buscarLucro(){
      /*if(this.isUserInRole('MASTER') || this.isUserInRole('SUPER')) {
       const retorno = await this.service.update(this.dataSelecionada);
       if(retorno.status.code == 1){
         this.lucrosData = retorno.value.operacoes;
         this.lucrosDataOriginal =  retorno.value.operacoes;
         this.ea.publish('EVT_IN_PLAY', retorno.value.inPlay);
         this.doSearch();
        }
      } */
        
   }
 
  detached(){
    if(this.interval){
      clearInterval(this.interval);
    }
    if(this.intervalSaldo){
      clearInterval(this.intervalSaldo);
    }
    if(this.intervalInplay){
      clearInterval(this.intervalInplay);
    }
  }

  fillDatePicker = (selected = new Date()) => {
    this.datePicker = flatpickr("#selectedDateLucroDiario", {
      altInput: true,
      altFormat: "F j, Y",
      dateFormat: "d-m-Y", 
      defaultDate: selected,
      // minDate: selected,
      disableMobile: true,
      static: this.onCheckWindowWidth(),
      "locale": Portuguese,
      onChange: (selectedDate, dateStr, instance) => {
        this.dataSelecionada = this.ajustarDia(selectedDate)
        this.setSession('inplay-date',this.dataSelecionada);
        this.buscarLucro();
      }
    });
  }

  ajustarDia(newDate = new Date()){
    var selectedDate = new Date(newDate);
    var dd = String(selectedDate.getDate()).padStart(2, '0');
    var mm = String(selectedDate.getMonth() + 1).padStart(2, '0');  
    var yyyy = selectedDate.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
  }

  onOpendDatePicker = () => {
    this.datePicker.open();
  }

  getCssRed(v){
    if(v.lost > 0){
      return "text-danger";
    }
    return "";
  } 
   
  removeDblClick(){
   $("div.modal-backdrop.fade.show").remove(); 
   return false;
  }

  async abrirOperacao(id){
    if(id == null){
      id = this.getProperty('tenant');
    }
    $('#container-operacao').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
    let retorno = await this.service.buscarOperacoes(id);
    $('#container-operacao').unblock(); 

    if(retorno.status.code == 1){
      this.operacoes = retorno.value.partidas;
      let modalOperacoes = new bootstrap.Modal(document.getElementById('modal-entradas'), {});
      modalOperacoes.show();
    }
  }
  
  getCssMeta(value){
    if(value.percentualDiario < 0){
      value.percentualDiario = (value.percentualDiario + "").replaceAll("-","");
      return "operacao-perda text-center "
    }
    return "operacao-lucro text-center";
  }

  getCssLucro(value){
    if(value.lucroReal == '.00'){  
      return "text-center";
    } 
    if(value.lucroReal.indexOf("-")  != -1 ){
      return "operacao-perda text-center "
    }
    return "operacao-lucro text-center";
  }
 
  getCssPerda(value){
    if(value.lucroReal.indexOf("-") != -1 ){
      return "operacao-perda text-center "
    }
    return "text-center";
  }

  getCssOperacao(value){
    if(value.emOperacao){
      return "em-operacao text-center";
    }
    return "text-center";
  }

  onRefresh = () => {
    console.log('refresh')
  }



  onSetCharts() {
    
    if ( this.chart==null) {
     this.chart = new Chart( document.getElementById("chLineLucro"), {
      type: 'line',
      options: {
        scales: { 
          x: {
            display: true,
            title: {
              display: true,
              text: 'Horário de entrada',
            }
          },
          y: {
            display: true,
            title: {
              display: true,
              text: 'Meu Capital',
            }
          },
          xAxes: [{
            ticks: {
              beginAtZero: true,
            },
            title: {
              display: true,
              text: 'Meu Capital',
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true,
      }
      });
    }
  }

}       
 