import { BasicView } from 'views/BasicView';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject} from 'aurelia-framework';
import Swal from 'sweetalert2'

@inject(EventAggregator)
export class Contas extends BasicView {
  contasData = [];
  contasDataOriginal = [];
  onEditing = false;
  conta={nome:"", email:"", celular:"", login:"", senha:"",token:"",precoFixo:false, ativo:false,
         valorFixoEntrada:0, percEntradaPadrao:0, operacaoAutomatica:false, meta:0, id:null,
         creditos:0, copyTips:false}
  mensagemErro ="";
  mensagemSucesso ="";
  
  valorFixo = [
    { id: true, name: 'Sim' },
    { id: false, name: 'Não' },
  ];

  operacao = [
    { id: true, name: 'Sim' },
    { id: false, name: 'Não' },
  ];

  selectedProductId = null;
  contaSelecionada = null;
  login;
  exibirBotaoAtivar = true;
  nomeConta= "";
  
  constructor(eventAggregator,...rest){
    super(...rest); 
    this.ea = eventAggregator;
  }
  tipoConta=[
            {id:2, value:"OPERADOR"},
            {id:3, value:"CLIENTE"},
            {id:4, value:"COCKPIT"},
            {id:1, value:"MASTER"}
            ]

  selectMatcher = (a, b) => a === b;

  attached() {
    this.buscarContas();
    this.login = this.getUsuario();
  } 

  perguntaHabilitarRobo(conta){
    this.contaSelecionada = conta;
    let mensagem = 'Deseja realmente ativar a conta ' + this.contaSelecionada.nome + ' para operação automática do robô? Essa operação precisa de um crédito para ser realizada.';
    Swal.fire({
      title: 'Habilitar robô automático',
      'text':mensagem, 
      showDenyButton: true,
      confirmButtonText: 'Sim, quero habilitar',
      denyButtonText: `Desistir`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.habilitarRoboAutomatico();
      } else if (result.isDenied) {
        //Swal.fire('Changes are not saved', '', 'info')
      }
    });
  }

  async habilitarRoboAutomatico(){
    $('#modalNewAccount').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    });

    let retorno = await this.service.habilitarRoboAutomatico(this.contaSelecionada.id);

    if(retorno.status.code == 1){
      Swal.fire({
        'title':'Robô automático habilitado',
        'text':'O robô automático foi habilitado com sucessso na conta ' + this.contaSelecionada.nome + ".", 
        icon: 'success'
      })
      this.contaSelecionada.pagouRobo = true;
    }else{
      Swal.fire({
        'title':'Créditos insuficientes',
        'text':retorno.status.message, 
        icon: 'error'
      });
    } 
    
    $('#modalNewAccount').unblock(); 
    this.buscarContas();
  }

  confirmarExcluirConta(conta){
    this.contaSelecionada = conta;
    let mensagem = 'Deseja realmente excluir a conta ' + this.contaSelecionada.nome + '? Essa operação não poderá ser desfeita.';
    Swal.fire({
      title: 'Exclusão de conta',
      'text':mensagem, 
      showDenyButton: true,
      confirmButtonText: 'Quero excluir',
      denyButtonText: `Desistir`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.excluirConta();
      } else if (result.isDenied) {
        //Swal.fire('Changes are not saved', '', 'info')
      }
    });
  }

  async excluirConta(){
    $('#table-contas').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    });

    let retorno = await this.service.excluirConta(this.contaSelecionada);

    if(retorno.status.code == 1){
      Swal.fire({
        'title':'Exlcusão de conta',
        'text':'A conta ' + this.contaSelecionada.nome + " foi excluída com sucesso.", 
        icon: 'success'
      })
    }else{
      Swal.fire({
        'title':'Exlcusão de conta',
        'text':retorno.status.message, 
        icon: 'error'
      });
    } 
    
    $('#table-contas').unblock(); 
    this.buscarContas();
  }

  async ativarConta(){
    $('#modaAtivarConta').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    });

    let retorno = await this.service.ativarConta(this.contaSelecionada)
    
    if(retorno.status.code == 1){
      this.login.creditos = retorno.value; 
      this.setSession('user', this.login);
      this.ea.publish("HANDLE_CREDITOS",{});
      this.mensagemAtivacaoConta = "Conta ativada com sucesso";
      this.buscarContas();
      Swal.fire({
        'title':'Ativação de conta',
        'text':'Essa conta foi ativada com sucesso', 
        icon: 'success'
      })

    }else{
      Swal.fire({
        'title':'Ativação de conta',
        'text':retorno.status.message, 
        icon: 'error'
      })
      this.mensagemAtivacaoConta = retorno.status.message;
    } 
  
    $('#modaAtivarConta').unblock(); 
  }

  async abrirContaAtivar(conta){
    this.exibirBotaoAtivar
    this.abrirModalAtivacao();
    this.buscarConta(conta);
  }

  async abrirCadastrarConta(conta){
    this.onOpenModalNewAccount();
    this.buscarConta(conta);
  }

  async buscarConta(conta){
    this.contaSelecionada = conta;
    $('#table-contas').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    });
    let retorno = await this.service.buscarConta(this.contaSelecionada.id);
    if(retorno.status.code == 1){
      this.contaSelecionada = retorno.value;
    }

    this.conta={nome:this.contaSelecionada.nome, email:this.contaSelecionada.email, 
          celular:this.contaSelecionada.telefone, login:this.contaSelecionada.login, 
          senha:this.contaSelecionada.senha,
          id:this.contaSelecionada.id,
          creditos:this.contaSelecionada.creditos,
          dataCadastro:this.contaSelecionada.dataCadastroLogin,
          token:this.contaSelecionada.accountKey,
          precoFixo:this.contaSelecionada.precoFixo, 
          ativo:this.contaSelecionada.ativo,
          valorFixoEntrada:this.contaSelecionada.valorFixoEntradaPadrao, 
          tipo:this.contaSelecionada.tipo,
          percEntradaPadrao:this.contaSelecionada.percEntradaPadrao, 
          pagouRobo:this.contaSelecionada.pagouRobo,
          copyTips:this.contaSelecionada.copyTips,
          operacaoAutomatica:this.contaSelecionada.operacaoAutomatica, meta:this.contaSelecionada.meta}
    $('#table-contas').unblock(); 
  }

  async cadastrarConta(){
    this.mensagemErro="";
    this.mensagemSucesso="";

    $("#labelNome").removeClass("col-form-label-red");
    $("#labelEmail").removeClass("col-form-label-red");
    $("#labelLogin").removeClass("col-form-label-red");
    $("#labelSenha").removeClass("col-form-label-red");
    $("#labelToken").removeClass("col-form-label-red");
    $("#labelCelular").removeClass("col-form-label-red");
    $("#labelMeta").removeClass("col-form-label-red");
    $("#labelValorFixo").removeClass("col-form-label-red");
    $("#labelPorcentagem").removeClass("col-form-label-red");

    $("#labelNome").addClass("col-form-label");
    $("#labelEmail").addClass("col-form-label");
    $("#labelLogin").addClass("col-form-label");
    $("#labelSenha").addClass("col-form-label");
    $("#labelToken").addClass("col-form-label");
    $("#labelCelular").addClass("col-form-label");
    $("#labelMeta").addClass("col-form-label");
    $("#labelValorFixo").addClass("col-form-label");
    $("#labelPorcentagem").addClass("col-form-label");

    let validacao = true;
    if(this.conta.nome==""){
      $("#labelNome").addClass("col-form-label-red");validacao = false;
    }
    if(this.conta.email==""){
      $("#labelEmail").addClass("col-form-label-red");validacao = false;
    }
    if(this.conta.login==""){
      $("#labelLogin").addClass("col-form-label-red");validacao = false;
    }
    if(this.conta.senha==""){
      $("#labelSenha").addClass("col-form-label-red");validacao = false;
    }
    if(this.conta.telefone==""){
      $("#labelCelular").addClass("col-form-label-red");validacao = false;
    }
    if(this.conta.operacaoAutomatica){
      if(this.conta.meta == ""){
        $("#labelMeta").addClass("col-form-label-red");validacao = false;
      }
    }
    if(this.conta.operacaoAutomatica){
      if(this.conta.precoFixo){
        if(this.conta.valorFixoEntrada == 0){
          $("#labelValorFixo").addClass("col-form-label-red");validacao = false;
        }
      }else{
        if(this.conta.percEntradaPadrao == 0){
          $("#labelPorcentagem").addClass("col-form-label-red");validacao = false;
        }
      }
    }
    this.mensagemSucesso = "";
    this.mensagemErro = "";

    if(validacao){
      $('#body-modal-conta').block({ 
        message: "<img style='width: 5rem;' src='img/ball.gif'>" 
      });
      let retorno = null; 
      if(this.conta.id != null){
        retorno = await this.service.atualizarContaCockpit(this.conta);
      }else{
        retorno = await this.service.cadastrarContaCockpit(this.conta);
      }
      $('#body-modal-conta').unblock(); 
      if(retorno.status.code == 1){
        if(this.conta.id != null){
          this.mensagemSucesso = "Conta atualizada com sucesso!";
        }else{
          this.mensagemSucesso = "Conta validada e criada com sucesso!";
        }
        Swal.fire({
          'title':'Cadastro de conta',
          'text':this.mensagemSucesso, 
          icon: 'success'
        })
        $('#btnCloseModaAccount').click();
      }else{
        this.mensagemErro = retorno.status.message;
        Swal.fire({
          'title':'Cadastro de conta',
          'text':this.mensagemErro, 
          icon: 'error'
        })
      }
      this.buscarContas();
    }else{
      this.mensagemErro="Informe os dados de cadastro corretamente.";
      Swal.fire({
        'title':'Dados inválidos',
        'text':this.mensagemErro, 
        icon: 'error'
      })
    }
  }

  async testarConta(){
    this.mensagemErro = "";
    this.mensagemSucesso = "";
    $("#labelLogin").removeClass("col-form-label-red");
    $("#labelSenha").removeClass("col-form-label-red");
    $("#labelToken").removeClass("col-form-label-red");

    $("#labelLogin").addClass("col-form-label");
    $("#labelSenha").addClass("col-form-label");
    $("#labelToken").addClass("col-form-label");

    if(this.conta.login == ""){$("#labelLogin").addClass("col-form-label-red");}
    if(this.conta.senha == ""){$("#labelSenha").addClass("col-form-label-red");}

    if(this.conta.login == "" || this.conta.senha == "" ){
      this.mensagemErro = "Informe os dados da conta betfair para testar o login";
      return
    }

    $('#body-modal-conta').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
    let retorno = await this.service.testarConta(this.conta);
    $('#body-modal-conta').unblock(); 
    if(retorno.status.code == 1){
      this.mensagemSucesso = "Conta validada com sucesso";
    }else{
      this.mensagemErro = retorno.status.message;
    }
  }

  doSearch(e){  
    this.contasData = [];   
    if(this.nomeConta == ""){  
      this.contasData = this.contasDataOriginal; 
    }else{
      this.contasData = this.contasDataOriginal.filter(i => (i.nome.toLowerCase().indexOf(this.nomeConta.toLowerCase()) != -1  || i.email.toLowerCase().indexOf(this.nomeConta.toLowerCase()) != -1 )   );
    }
    return true;
  }

  async buscarContas() {
    /*let retorno = await this.service.buscarLogins();
    if(retorno.status.code == 1){
      this.contasData = retorno.value;
    }

    this.contasDataOriginal =  this.contasData;
    */
  }

  onEditData = () => {
    this.onEditing = !this.onEditing;
  }

  getCssConta(conta){
    if(!conta.ativo){
      return "sem-token"
    }
    return "";
  }

  abrirModalAtivacao(){
    let modalAddOdds = new bootstrap.Modal(document.getElementById('modaAtivarConta'), {});
    modalAddOdds.show();
  }

  onOpenModalNewAccount(){
    this.mensagemErro = "";
    this.mensagemSucesso = "";
    $("#labelNome").removeClass("col-form-label-red");
    $("#labelEmail").removeClass("col-form-label-red");
    $("#labelLogin").removeClass("col-form-label-red");
    $("#labelSenha").removeClass("col-form-label-red");
    $("#labelToken").removeClass("col-form-label-red");
    $("#labelCelular").removeClass("col-form-label-red");
    $("#labelMeta").removeClass("col-form-label-red");
    $("#labelValorFixo").removeClass("col-form-label-red");
    $("#labelPorcentagem").removeClass("col-form-label-red");

    $("#labelNome").removeClass("col-form-label");
    $("#labelEmail").removeClass("col-form-label");
    $("#labelLogin").removeClass("col-form-label");
    $("#labelSenha").removeClass("col-form-label");
    $("#labelToken").removeClass("col-form-label");
    $("#labelCelular").removeClass("col-form-label");
    $("#labelMeta").removeClass("col-form-label");
    $("#labelValorFixo").removeClass("col-form-label");
    $("#labelPorcentagem").removeClass("col-form-label");
    this.conta={nome:"", email:"", celular:"", login:"", senha:"",token:"",precoFixo:false, ativo:false,
                valorFixoEntrada:0, percEntradaPadrao:0, operacaoAutomatica:false, meta:0,
                copyTips:false}
    let modalAddOdds = new bootstrap.Modal(document.getElementById('modalNewAccount'), {});
    modalAddOdds.show();
  }
}
