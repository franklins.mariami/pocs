import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {Service} from 'services/service';
import {PLATFORM} from 'aurelia-pal';
import {BasicView} from 'views/BasicView';
import flatpickr from "flatpickr";
import { Portuguese } from "flatpickr/dist/l10n/pt.js"
import {EventAggregator} from 'aurelia-event-aggregator';
import {StorageUtil} from "util/storage-util";
import Swal from 'sweetalert2';

@inject(Service,Router,EventAggregator,StorageUtil)
export class Home extends BasicView {

  inPlay;
  inPlayFiltrado; 
  atualizadoEm="";
  exibindoDetalhes = false;
  timeToConfirm=false;
  inPlaySelecionado = null;
  timeuOutCashout;
  interval;
  login;
  listaPartidaScoreBing=[];

  inPlayOptions=[{id:0,value:"ABERTA"},{id:1,value:"GREEN"},{id:2,value:"RED"},{id:3,value:"TODAS"}]
  inPlayOptionsSelecionado = 3;
  entradaSelecionada = null;
  
  constructor(service,router, eventAggregator, storageUtil, ...rest){
    super(...rest);
    this.service = service;
    this.router = router;
    this.ea = eventAggregator;
    this.storageUtil = storageUtil;
  }  
  
  attached(){
    this.selectedDate = this.ajustarDia();
    this.fillDatePicker();
    this.eventsSubscribe();
    this.login = this.getUsuario();
  }

  zerarCalculosEntrada(entrada){
    if((entrada.aumentoValorEntrada == "" && entrada.aumentoEntradaPorcentagem == "" ) ||
       (entrada.aumentoValorEntrada == null && entrada.aumentoEntradaPorcentagem == null )  ){
      entrada.novoSaldo = 0;
      entrada.novoValorEntrada = 0;
      entrada.novoLucro = 0;
    }
  }

  calcularNovaEntradaValor(entrada){
    this.zerarCalculosEntrada(entrada);
    if(entrada.aumentoValorEntrada > 0){
      entrada.aumentoEntradaPorcentagem="";
      this.entradaSelecionada = entrada;
      this.entradaSelecionada.novoSaldo = parseFloat(entrada.saldo - entrada.aumentoValorEntrada).toFixed(2);
      this.entradaSelecionada.novoValorEntrada = entrada.aumentoValorEntrada;
      this.entradaSelecionada.novoLucro = parseFloat((entrada.novaOdd * entrada.novoValorEntrada)- entrada.novoValorEntrada).toFixed(2);
    }
  }

  calcularNovaEntradaPercentagem(entrada){
    this.zerarCalculosEntrada(entrada);
    if(entrada.aumentoEntradaPorcentagem > 0){
      entrada.aumentoValorEntrada="";
      this.entradaSelecionada = entrada;
      let valorEntrada = parseFloat((entrada.aumentoEntradaPorcentagem / 100.0 ) * entrada.saldo ).toFixed(2);
      this.entradaSelecionada.novoSaldo = parseFloat(entrada.saldo - valorEntrada).toFixed(2);
      this.entradaSelecionada.novoValorEntrada = valorEntrada;
      this.entradaSelecionada.novoLucro = parseFloat((entrada.novaOdd * entrada.novoValorEntrada) - entrada.novoValorEntrada).toFixed(2);
    }
  }

  aumentarStackConta(entrada){
    this.entradaSelecionada = entrada;
    if((this.entradaSelecionada.aumentoEntradaPorcentagem  == "" && this.entradaSelecionada.aumentoValorEntrada == "") ||
      (this.entradaSelecionada.aumentoEntradaPorcentagem  == null && this.entradaSelecionada.aumentoValorEntrada == null)){
      Swal.fire({
        title: 'Aumento de stack',
        'html':"Informe o <span style='color:red'><strong>percentual</strong></span> para cálculo ou um <span style='color:red'><strong>valor fixo</strong></span> para aumentar a stack da conta <strong>" + this.entradaSelecionada.login.nome +"</strong>.", 
        confirmButtonText: 'Fechar',
      });
      return ;
    }
    
    let mensagem = "Você tem certeza que deseja <span style='color:red'><strong>aumentar</strong></span> mais <strong> R$ " + this.entradaSelecionada.aumentoValorEntrada + " reais</strong> para a conta <strong>" + this.entradaSelecionada.login.nome +"</strong>" +"?";
    if(this.entradaSelecionada.aumentoEntradaPorcentagem > 0){
      mensagem = "Você tem certeza que deseja <span style='color:red'><strong>aumentar</strong></span> mais <strong> " + this.entradaSelecionada.aumentoEntradaPorcentagem + "%</strong> para a conta <strong>" + this.entradaSelecionada.login.nome +"</strong>" +"?";
    }
    if(this.entradaSelecionada.novoValorEntrada < 5){
      Swal.fire({
        title: 'Aumento de stack',
        'html':"O <span style='color:red'><strong>valor de entrada</strong></span> não pode ser inferior à <span style='color:red'><strong>R$ 5,00 reais.</strong></span>", 
        confirmButtonText: 'Fechar',
      });
      return;
    }
    Swal.fire({
      title: 'Aumento de stack',
      'html':mensagem, 
      showDenyButton: true,
      confirmButtonText: 'Sim, aumentar',
      denyButtonText: `Desistir`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.confirmarAumentarStackConta();
      }  
    });
  }

  aumentarStackTotalContas(){

    if(this.aumentoValorEntrada == "" && this.aumentoEntradaPorcentagem == ""){
      Swal.fire({
        title: 'Aumento de stack',
        'html':"Informe o <span style='color:red'><strong>percentual</strong></span> para cálculo ou um <span style='color:red'><strong>valor fixo</strong></span> para aumentar a stack.", 
        confirmButtonText: 'Fechar',
      });
      return ;
    }
    
    let mensagem = "Você tem certeza que deseja <span style='color:red'><strong>aumentar</strong></span> mais <strong> R$ " + this.aumentoValorEntrada + " reais</strong> em todas as contas disponíveis?";
    if(this.aumentoEntradaPorcentagem > 0){
      mensagem = "Você tem certeza que deseja <span style='color:red'><strong>aumentar</strong></span> mais <strong> " + this.aumentoEntradaPorcentagem + "%</strong> em todas as contas disponíveis?";
    }

    Swal.fire({
      title: 'Aumento de stack',
      'html':mensagem, 
      showDenyButton: true,
      confirmButtonText: 'Sim, aumentar',
      denyButtonText: `Desistir`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.confirmarAumentarStackTotal();
      }  
    });
  }
  
  confirmarAumentarStackTotal(){
    this.aumentarStackTotal(this.entradasRealizadas);
  }

  confirmarAumentarStackConta(){
    let contas = [];
    contas.push(this.entradaSelecionada);
    this.aumentarStackTotal(contas);
  }

 async aumentarStackTotal(contas){
    this.block('modal-aumento-aposta');
    let retorno = await this.service.aumentarStack(contas);
    this.unblock('modal-aumento-aposta');
    if(retorno.status.code == 1){
      Swal.fire({
        title: 'Aumento de stack',
        'html':"Ação realizada com sucesso. <span style='color:red'><Strong>Aguarde</strong></span> alguns instantes para que as entradas sejam realizadas.", 
        confirmButtonText: 'Fechar',
      });
    }else{
      Swal.fire({
        title: 'Aumento de stack',
        'html':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
 }

 async buscarOdd(){
    this.block('modal-aumento-aposta');
    let entrada = this.entradasRealizadas[0];
    let retorno = await this.service.buscarOddPorEntrada(entrada.id);
    this.unblock('modal-aumento-aposta');
    if(retorno.status.code == 1){
      this.entradasRealizadas.forEach(e=>{
        e.novaOdd=retorno.value;
        e.novoLucro = parseFloat((e.novaOdd * e.novoValorEntrada)-e.novoValorEntrada).toFixed(2);
        if(retorno.value == 0.0){
          Swal.fire({
            title: 'Aumento de stack',
            'html':"Esse mercado não está mais disponível", 
            confirmButtonText: 'Fechar',
          });
        }
      });
    }else{
      Swal.fire({
        title: 'Aumento de stack',
        'html':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
 }

 async abriAumentoAposta(inPlay){
    this.entradaSelecionada = null;
    this.aumentoValorEntrada = "";
    this.aumentoEntradaPorcentagem = "";

    this.inPlaySelecionado = inPlay;
    let retorno = await this.service.buscarEntradasRealizadas(inPlay.idPartida,
                                                              inPlay.idMercado,
                                                              inPlay.idSelection,);
    if(retorno.status.code == 1){
      this.entradasRealizadas = retorno.value;
      
      this.entradasRealizadas.forEach(e=>{
        e.saldo = parseFloat(e.saldo).toFixed(2);
      });

      await this.buscarOdd();
      let modalAddOdds = new bootstrap.Modal(document.getElementById('modal-aumento-aposta'), {});
      modalAddOdds.show();
    }else{
      Swal.fire({
        title: 'Aumento de stack',
        'html':retorno.status.message, 
        confirmButtonText: 'Fechar',
      });
    }
  }
  
  zerarCalculos(){
    if(this.aumentoValorEntrada == "" && this.aumentoEntradaPorcentagem == ""){
      this.entradasRealizadas.forEach(e=>{
        e.novoSaldo = 0;
        e.novoValorEntrada = 0;
        e.novoLucro = 0;
        e.aumentoEntradaPorcentagem = "";
        e.aumentoValorEntrada = "";
      });
    }
  }

  setarValorAumento(){
    this.zerarCalculos();
    if(this.aumentoValorEntrada > 0){
      if(this.aumentoValorEntrada == "" && this.aumentoEntradaPorcentagem == ""){
        return;
      }
        this.aumentoEntradaPorcentagem = "";
      

      this.entradasRealizadas.forEach(e=>{
        e.aumentoEntradaPorcentagem="";
        e.aumentoValorEntrada = this.aumentoValorEntrada;
        e.novoSaldo = parseFloat(e.saldo - this.aumentoValorEntrada).toFixed(2);
        e.novoValorEntrada = this.aumentoValorEntrada;
        e.novoLucro = parseFloat((e.novaOdd * e.novoValorEntrada) - e.novoValorEntrada).toFixed(2) ;
      });
    }
  }

  setarValorPorcentagemAumento(){
    this.zerarCalculos();
    if(this.aumentoEntradaPorcentagem > 0){
      if(this.aumentoValorEntrada == "" && this.aumentoEntradaPorcentagem == ""){
        return;
      }
        this.aumentoValorEntrada = "";
    
      this.entradasRealizadas.forEach(e=>{
        e.aumentoEntradaPorcentagem=this.aumentoEntradaPorcentagem;
        e.aumentoValorEntrada = "";
        let valorEntrada = parseFloat((this.aumentoEntradaPorcentagem / 100.0 ) * e.saldo ).toFixed(2);
        e.novoSaldo = parseFloat(e.saldo - valorEntrada).toFixed(2);
        e.novoValorEntrada = valorEntrada;
        e.novoLucro = parseFloat((e.novaOdd * e.novoValorEntrada) - e.novoValorEntrada).toFixed(2);
      });
    }
  }

  onLogout = () => {
    this.navigate("/");
  }

  async abrirEntradas(inPlay){
    this.cancelarca = inPlay;
    this.inPlaySelecionado = inPlay;
    let retorno = await this.service.buscarEntradasRealizadas(inPlay.idPartida,
                                                                          inPlay.idMercado,
                                                                          inPlay.idSelection,);
    
   if(retorno.status.code == 1){
      this.entradasRealizadas = retorno.value;
   }
   this.exibindoDetalhes = true;
  }

  abrirDuvida(){
    this.navigate("ajuda");
  }

  getIdCard(id){
    return "card-in-play-" + id;
  }

  onShowTimeToConfirm = () => {
    this.timeToConfirm = 5;
    this.interval = setInterval(() => {
      this.timeToConfirm -= 1
    }, 1000);

    this.timeuOutCashout = setTimeout(() => {
      this.fecharEntradasCashout();
    }, 5000);
  }
 
  cancelarCashOut = () => {
    if(this.timeuOutCashout){  
      clearTimeout(this.timeuOutCashout); 
    }
    if(this.interval){
      clearInterval(this.interval);
    }
    this.timeToConfirm = 0;   
    this.exibindoDetalhes = false;
    this.inPlaySelecionado = null;
  }

  fecharEntradasCashout(){
    if(this.timeuOutCashout){
      clearTimeout(this.timeuOutCashout);
    }
    if(this.interval){
      clearInterval(this.interval);
    }
    this.timeToConfirm = 0;
    this.exibindoDetalhes = false;
    this.realizarCashOut();
  }

  async realizarCashOut(){
    $('#following-matches').block({ 
      message: "<img style='width: 5rem;' src='img/ball.gif'>" 
    }); 
    let retorno = await this.service.cashOut(this.cancelarca.idPartida,
                                            this.cancelarca.idMercado,
                                            this.cancelarca.idSelection);
    if(retorno.status.code == 0){
      console.log("ERRO NO CASHOUT");
    }
    this.entradasRealizadas = retorno.value;
    $('#following-matches').unblock(); 
  }
 
  abrirPopup(){
    $('#my-toast').toast()
  } 
  
  traduzirTempo(t){
    t =t.replaceAll("Today", "Hoje");
    t =t.replaceAll("END", "FIM"); 
    t =t.replaceAll("In-Play", "Ao Vivo");
    t =t.replaceAll("Starting soon", "Em breve");
    t =t.replaceAll("HT", "INT"); 
    t =t.replaceAll("NS", "-"); 
    t =t.replaceAll("Starting in", "Em");
    return t;
  }

  abrirLinkBet365(p){
    if(p.t !='END'){
      window.open(p.linkBet365,'_blank')
    }
  }

  abrirLinkBetFair(p){
    if(p.t !='END'){
      window.open(p.linkBetFair,'_blank')
    }
  }

  abrirLink(link){
    window.open(link,'_blank')
  }
 
  processarCockpit(message){
    let mensagem = JSON.parse(message.value);
    if (mensagem.tipo == "ESTATISTICA_PARTIDA"){
      if(this.inPlayFiltrado && this.inPlayFiltrado != null){
        this.inPlayFiltrado.forEach(p=>{
          mensagem.value.itens.forEach(score =>{
            if(p.idPartida == score.idPartida && p.tempo !='FIM' ){
              if(!p.scoreBing){
                p.scoreBing = {}
              }
              p.scoreBing.plus = score.plus;
              p.scoreBing.rd = score.rd;
              p.scoreBing.sd = score.sd;
              let filtros = this.listaPartidaScoreBing.filter(i => {return i.idPartida != p.idPartida });
              this.listaPartidaScoreBing = filtros;
              this.listaPartidaScoreBing.push(p);
            }
          });
        });
      }
    } 
  } 

  eventsSubscribe(){
    this.subscriberInplay = this.ea.subscribe('EVT_IN_PLAY', (value)=>{
       this.processarInPlay(value);
    });
    this.subscriberUpdate = this.ea.subscribe('HANDLE_PAINEL_STATUS', (value)=>{
      this.atualizarHorario(value);
    }); 
    this.subscriberCredito = this.ea.subscribe('HANDLE_CREDITOS', (value)=>{
      this.atualizarCredito(value);
    }); 
    this.subscriberCp = this.ea.subscribe('HANDLE_CP_MESSAGE', (message)=>{
      this.processarCockpit(message);
   });
  }

  ajustarTitle(entrada){
    let operador = entrada.loginOperador == null ? "Automático" : entrada.loginOperador.nome; 
    return entrada.horarioEntrada + " - "+  operador;
  }

  processarInPlay(value){ 
    
    this.inPlayFull = value;
    this.inPlayFiltrado = [];
    let listaAux = [];

    this.inPlayFull.forEach(inPlay=>{
      let filtrado = listaAux.filter(item => item.casa == inPlay.casa && item.idMercado == inPlay.idMercado && item.idSelection == inPlay.idSelection);
      if(filtrado.length == 0){
        this.getCssCard(inPlay);
      
        let filtros = this.listaPartidaScoreBing.filter(i => {return i.idPartida == inPlay.idPartida });
        if(filtros.length > 0){
          inPlay.scoreBing =  filtros[0].scoreBing;
        }

        listaAux.push(inPlay);
      }
    });
    //listaAux = listaAux.sort((a, b) => (Number(a.finalizado) - Number(b.finalizado)));
    this.inPlayFiltrado.push(...this.inPlayFull);
  } 
  
  atualizarCredito(){
    this.login = this.getUsuario();
  }

  atualizarHorario(v){
    var data = new Date();
    this.atualizadoEm = data.getDate() + "/" + (parseInt(data.getMonth()) + 1) + "/" + data.getFullYear() + " " + data.getHours() + ":" + data.getMinutes() + ":" + data.getSeconds();
  }

  detached(){
    if(this.subscriberCredito){
      this.subscriberCredito.dispose();
    }
    if(this.subscriberUpdate){
      this.subscriberUpdate.dispose();
    }
    if(this.subscriberInplay){
      this.subscriberInplay.dispose();
    }
    if(this.subscriberCp){
      this.subscriberCp.dispose();
    }
  }

  fillDatePicker = (selected = new Date()) => {
    this.datePicker = flatpickr("#selectedDate", {
      altInput: true, 
      altFormat: "F j, Y",
      dateFormat: "d-m-Y",
      defaultDate: selected,
      // minDate: selected,
      disableMobile: true,
      static: this.onCheckWindowWidth(),
      "locale": Portuguese,
      onChange: (selectedDate, dateStr, instance) => {
        this.selectedDate = this.ajustarDia(selectedDate);
        this.publicarNovaData(this.selectedDate);
      }
    });
  }

  publicarNovaData(data){
      this.ea.publish("EVT_TROCAR_DIA",data);
  } 

  ajustarDia(newDate = new Date()){
    var selectedDate = new Date(newDate);
    var dd = String(selectedDate.getDate()).padStart(2, '0');
    var mm = String(selectedDate.getMonth() + 1).padStart(2, '0');  
    var yyyy = selectedDate.getFullYear();
    return dd + '/' + mm + '/' + yyyy;
  }

  onOpendDatePicker = () => {
    this.datePicker.open();
  }
  
  getCssCard(card){
    card.finalizado = false;
    let comparador ='END';
   
    if(card.ht){
      comparador ='HT';
      
      if((card.golsPrimeiroTempo >= 2 && card.mercado == 'MENOS DE 15 GOLS' && card.segundoTempo) || 
        (card.golsPrimeiroTempo >= 3 && card.mercado == 'MENOS DE 25 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 4 && card.mercado == 'MENOS DE 35 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 5 && card.mercado == 'MENOS DE 45 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 6 && card.mercado == 'MENOS DE 55 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 7 && card.mercado == 'MENOS DE 65 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 8 && card.mercado == 'MENOS DE 75 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 9 && card.mercado == 'MENOS DE 85 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo == 0 && card.mercado == 'MAIS DE 05 GOLS'  && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 1 && card.mercado == 'MAIS DE 15 GOLS'  && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 2 && card.mercado == 'MAIS DE 25 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 3 && card.mercado == 'MAIS DE 35 GOLS'  && card.segundoTemSpo) ||
        (card.golsPrimeiroTempo <= 4 && card.mercado == 'MAIS DE 45 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 5 && card.mercado == 'MAIS DE 55 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 6 && card.mercado == 'MAIS DE 65 GOLS'  && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 7 && card.mercado == 'MAIS DE 75 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 8 && card.mercado == 'MAIS DE 85 GOLS'  && card.segundoTempo)){ 
        card.tipoInPlay = 2;
        return "match-card card-inplay-red shadow-in-play ";
      }
      
      if((card.golsPrimeiroTempo >= 1 && card.mercado == 'MAIS DE 05 GOLS' ) || 
        (card.golsPrimeiroTempo >= 2 && card.mercado == 'MAIS DE 15 GOLS' ) || 
        (card.golsPrimeiroTempo >= 3 && card.mercado == 'MAIS DE 25 GOLS' ) ||
        (card.golsPrimeiroTempo >= 4 && card.mercado == 'MAIS DE 35 GOLS' ) || 
        (card.golsPrimeiroTempo >= 5 && card.mercado == 'MAIS DE 45 GOLS' ) ||
        (card.golsPrimeiroTempo >= 6 && card.mercado == 'MAIS DE 55 GOLS' ) ||
        (card.golsPrimeiroTempo >= 7 && card.mercado == 'MAIS DE 65 GOLS' ) ||
        (card.golsPrimeiroTempo >= 8 && card.mercado == 'MAIS DE 75 GOLS' ) ||
        (card.golsPrimeiroTempo >= 9 && card.mercado == 'MAIS DE 85 GOLS' ) || 
        (card.golsPrimeiroTempo <= 0 && card.mercado == 'MENOS DE 05 GOLS' && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 1 && card.mercado == 'MENOS DE 15 GOLS' && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 2 && card.mercado == 'MENOS DE 25 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 3 && card.mercado == 'MENOS DE 35 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 4 && card.mercado == 'MENOS DE 45 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 5 && card.mercado == 'MENOS DE 55 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 6 && card.mercado == 'MENOS DE 65 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 7 && card.mercado == 'MENOS DE 75 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 8 && card.mercado == 'MENOS DE 85 GOLS' && card.segundoTempo)){ 
        card.finalizado = true;
        card.tipoInPlay = 1;
        return "match-card card-inplay-green shadow-in-play ";
      }
      card.tipoInPlay = 0;
      return "match-card shadow-in-play ";
    }else{

      if((card.g == 0 && card.mercado == 'MAIS DE 05 GOLS' && card.t != comparador) || 
        (card.g <= 1 && card.mercado == 'MAIS DE 15 GOLS' && card.t != comparador ) || 
        (card.g <= 2 && card.mercado == 'MAIS DE 25 GOLS' && card.t != comparador ) ||
        (card.g <= 3 && card.mercado == 'MAIS DE 35 GOLS' && card.t != comparador ) ||
        (card.g <= 4 && card.mercado == 'MAIS DE 45 GOLS' && card.t != comparador ) ||
        (card.g <= 5 && card.mercado == 'MAIS DE 55 GOLS' && card.t != comparador ) ||
        (card.g <= 6 && card.mercado == 'MAIS DE 65 GOLS' && card.t != comparador ) || 
        (card.g <= 7 && card.mercado == 'MAIS DE 75 GOLS' && card.t != comparador ) ||
        (card.g <= 8 && card.mercado == 'MAIS DE 85 GOLS' && card.t != comparador )){
          card.tipoInPlay = 0;
          return "match-card card-inplay-blue shadow-in-play ";
      }
  
      if((card.g >= 1 && card.mercado == 'MENOS DE 05 GOLS' && card.t == comparador ) || 
         (card.g >= 2 && card.mercado == 'MENOS DE 15 GOLS' && card.t == comparador ) || 
        (card.g >= 3 && card.mercado == 'MENOS DE 25 GOLS' && card.t == comparador ) ||
        (card.g >= 4 && card.mercado == 'MENOS DE 35 GOLS' && card.t == comparador ) ||
        (card.g >= 5 && card.mercado == 'MENOS DE 45 GOLS' && card.t == comparador ) ||
        (card.g >= 6 && card.mercado == 'MENOS DE 55 GOLS' && card.t == comparador ) ||
        (card.g >= 7 && card.mercado == 'MENOS DE 65 GOLS' && card.t == comparador ) ||
        (card.g >= 8 && card.mercado == 'MENOS DE 75 GOLS' && card.t == comparador ) ||
        (card.g >= 9 && card.mercado == 'MENOS DE 85 GOLS' && card.t == comparador ) ||
        (card.g == 0 && card.mercado == 'MAIS DE 05 GOLS'  && card.t == comparador) || 
        (card.g <= 1 && card.mercado == 'MAIS DE 15 GOLS'  && card.t == comparador) || 
        (card.g <= 2 && card.mercado == 'MAIS DE 25 GOLS'  && card.t == comparador) ||
        (card.g <= 3 && card.mercado == 'MAIS DE 35 GOLS'  && card.t == comparador) ||
        (card.g <= 4 && card.mercado == 'MAIS DE 45 GOLS'  && card.t == comparador) ||
        (card.g <= 5 && card.mercado == 'MAIS DE 55 GOLS'  && card.t == comparador) ||
        (card.g <= 6 && card.mercado == 'MAIS DE 65 GOLS'  && card.t == comparador) || 
        (card.g <= 7 && card.mercado == 'MAIS DE 75 GOLS'  && card.t == comparador) ||
        (card.g <= 8 && card.mercado == 'MAIS DE 85 GOLS'  && card.t == comparador)){ 
          card.tipoInPlay = 2;
        return "match-card card-inplay-red shadow-in-play ";
      }
       
      if((card.g >= 1 && card.mercado == 'MAIS DE 05 GOLS' ) || 
        (card.g >= 2 && card.mercado == 'MAIS DE 15 GOLS' ) || 
        (card.g >= 3 && card.mercado == 'MAIS DE 25 GOLS' ) ||
        (card.g >= 4 && card.mercado == 'MAIS DE 35 GOLS' ) || 
        (card.g >= 5 && card.mercado == 'MAIS DE 45 GOLS' ) ||
        (card.g >= 6 && card.mercado == 'MAIS DE 55 GOLS' ) ||
        (card.g >= 7 && card.mercado == 'MAIS DE 65 GOLS' ) ||
        (card.g >= 8 && card.mercado == 'MAIS DE 75 GOLS' ) ||
        (card.g >= 9 && card.mercado == 'MAIS DE 85 GOLS' ) || 
        (card.g == 0 && card.mercado == 'MENOS DE 05 GOLS' && card.t == comparador ) || 
        (card.g <= 1 && card.mercado == 'MENOS DE 15 GOLS' && card.t == comparador ) || 
        (card.g <= 2 && card.mercado == 'MENOS DE 25 GOLS' && card.t == comparador ) ||
        (card.g <= 3 && card.mercado == 'MENOS DE 35 GOLS' && card.t == comparador ) ||
        (card.g <= 4 && card.mercado == 'MENOS DE 45 GOLS' && card.t == comparador ) ||
        (card.g <= 5 && card.mercado == 'MENOS DE 55 GOLS' && card.t == comparador ) ||
        (card.g <= 6 && card.mercado == 'MENOS DE 65 GOLS' && card.t == comparador ) ||
        (card.g <= 7 && card.mercado == 'MENOS DE 75 GOLS' && card.t == comparador ) ||
        (card.g <= 8 && card.mercado == 'MENOS DE 85 GOLS' && card.t == comparador )){ 
        card.finalizado = true;
        card.tipoInPlay = 1;
        return "match-card card-inplay-green shadow-in-play ";
      }
      card.tipoInPlay = 0;
      return "match-card shadow-in-play ";
    }
  }

  onGetDetailsCSS(card){
    card.finalizado = false;
    let comparador = 'END';

    if(card.ht){
      comparador ='HT';
      if((card.golsPrimeiroTempo >= 2 && card.mercado == 'MENOS DE 15 GOLS' && card.segundoTempo) || 
        (card.golsPrimeiroTempo >= 3 && card.mercado == 'MENOS DE 25 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 4 && card.mercado == 'MENOS DE 35 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 5 && card.mercado == 'MENOS DE 45 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 6 && card.mercado == 'MENOS DE 55 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 7 && card.mercado == 'MENOS DE 65 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 8 && card.mercado == 'MENOS DE 75 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo >= 9 && card.mercado == 'MENOS DE 85 GOLS' && card.segundoTempo ) ||
        (card.golsPrimeiroTempo == 0 && card.mercado == 'MAIS DE 05 GOLS'  && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 1 && card.mercado == 'MAIS DE 15 GOLS'  && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 2 && card.mercado == 'MAIS DE 25 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 3 && card.mercado == 'MAIS DE 35 GOLS'  && card.segundoTemSpo) ||
        (card.golsPrimeiroTempo <= 4 && card.mercado == 'MAIS DE 45 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 5 && card.mercado == 'MAIS DE 55 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 6 && card.mercado == 'MAIS DE 65 GOLS'  && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 7 && card.mercado == 'MAIS DE 75 GOLS'  && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 8 && card.mercado == 'MAIS DE 85 GOLS'  && card.segundoTempo)){
        return "inplay-red";
      }
      
      if((card.golsPrimeiroTempo >= 1 && card.mercado == 'MAIS DE 05 GOLS' ) || 
        (card.golsPrimeiroTempo >= 2 && card.mercado == 'MAIS DE 15 GOLS' ) || 
        (card.golsPrimeiroTempo >= 3 && card.mercado == 'MAIS DE 25 GOLS' ) ||
        (card.golsPrimeiroTempo >= 4 && card.mercado == 'MAIS DE 35 GOLS' ) || 
        (card.golsPrimeiroTempo >= 5 && card.mercado == 'MAIS DE 45 GOLS' ) ||
        (card.golsPrimeiroTempo >= 6 && card.mercado == 'MAIS DE 55 GOLS' ) ||
        (card.golsPrimeiroTempo >= 7 && card.mercado == 'MAIS DE 65 GOLS' ) ||
        (card.golsPrimeiroTempo >= 8 && card.mercado == 'MAIS DE 75 GOLS' ) ||
        (card.golsPrimeiroTempo >= 9 && card.mercado == 'MAIS DE 85 GOLS' ) || 
        (card.golsPrimeiroTempo <= 0 && card.mercado == 'MENOS DE 05 GOLS' && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 1 && card.mercado == 'MENOS DE 15 GOLS' && card.segundoTempo) || 
        (card.golsPrimeiroTempo <= 2 && card.mercado == 'MENOS DE 25 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 3 && card.mercado == 'MENOS DE 35 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 4 && card.mercado == 'MENOS DE 45 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 5 && card.mercado == 'MENOS DE 55 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 6 && card.mercado == 'MENOS DE 65 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 7 && card.mercado == 'MENOS DE 75 GOLS' && card.segundoTempo) ||
        (card.golsPrimeiroTempo <= 8 && card.mercado == 'MENOS DE 85 GOLS' && card.segundoTempo)){ 
        card.finalizado = true;
        return "inplay-green";
      }
      return "inplay-white";
    }else{

      if((card.g == 0 && card.mercado == 'MAIS DE 05 GOLS' && card.t != comparador) || 
        (card.g <= 1 && card.mercado == 'MAIS DE 15 GOLS' && card.t != comparador ) || 
        (card.g <= 2 && card.mercado == 'MAIS DE 25 GOLS' && card.t != comparador ) ||
        (card.g <= 3 && card.mercado == 'MAIS DE 35 GOLS' && card.t != comparador ) ||
        (card.g <= 4 && card.mercado == 'MAIS DE 45 GOLS' && card.t != comparador ) ||
        (card.g <= 5 && card.mercado == 'MAIS DE 55 GOLS' && card.t != comparador ) ||
        (card.g <= 6 && card.mercado == 'MAIS DE 65 GOLS' && card.t != comparador ) || 
        (card.g <= 7 && card.mercado == 'MAIS DE 75 GOLS' && card.t != comparador ) ||
        (card.g <= 8 && card.mercado == 'MAIS DE 85 GOLS' && card.t != comparador )){
          return "inplay-blue";
      }
  
      if((card.g >= 2 && card.mercado == 'MENOS DE 15 GOLS' && card.t == comparador ) || 
        (card.g >= 3 && card.mercado == 'MENOS DE 25 GOLS' && card.t == comparador ) ||
        (card.g >= 4 && card.mercado == 'MENOS DE 35 GOLS' && card.t == comparador ) ||
        (card.g >= 5 && card.mercado == 'MENOS DE 45 GOLS' && card.t == comparador ) ||
        (card.g >= 6 && card.mercado == 'MENOS DE 55 GOLS' && card.t == comparador ) ||
        (card.g >= 7 && card.mercado == 'MENOS DE 65 GOLS' && card.t == comparador ) ||
        (card.g >= 8 && card.mercado == 'MENOS DE 75 GOLS' && card.t == comparador ) ||
        (card.g >= 9 && card.mercado == 'MENOS DE 85 GOLS' && card.t == comparador ) ||
        (card.g == 0 && card.mercado == 'MAIS DE 05 GOLS'  && card.t == comparador) || 
        (card.g <= 1 && card.mercado == 'MAIS DE 15 GOLS'  && card.t == comparador) || 
        (card.g <= 2 && card.mercado == 'MAIS DE 25 GOLS'  && card.t == comparador) ||
        (card.g <= 3 && card.mercado == 'MAIS DE 35 GOLS'  && card.t == comparador) ||
        (card.g <= 4 && card.mercado == 'MAIS DE 45 GOLS'  && card.t == comparador) ||
        (card.g <= 5 && card.mercado == 'MAIS DE 55 GOLS'  && card.t == comparador) ||
        (card.g <= 6 && card.mercado == 'MAIS DE 65 GOLS'  && card.t == comparador) || 
        (card.g <= 7 && card.mercado == 'MAIS DE 75 GOLS'  && card.t == comparador) ||
        (card.g <= 8 && card.mercado == 'MAIS DE 85 GOLS'  && card.t == comparador)){ 
        return "inplay-red";
      }
      if((card.g >= 1 && card.mercado == 'MAIS DE 05 GOLS' ) || 
        (card.g >= 2 && card.mercado == 'MAIS DE 15 GOLS' ) || 
        (card.g >= 3 && card.mercado == 'MAIS DE 25 GOLS' ) ||
        (card.g >= 4 && card.mercado == 'MAIS DE 35 GOLS' ) || 
        (card.g >= 5 && card.mercado == 'MAIS DE 45 GOLS' ) ||
        (card.g >= 6 && card.mercado == 'MAIS DE 55 GOLS' ) ||
        (card.g >= 7 && card.mercado == 'MAIS DE 65 GOLS' ) ||
        (card.g >= 8 && card.mercado == 'MAIS DE 75 GOLS' ) ||
        (card.g >= 9 && card.mercado == 'MAIS DE 85 GOLS' ) || 
        (card.g == 0 && card.mercado == 'MENOS DE 05 GOLS' && card.t == comparador ) || 
        (card.g <= 1 && card.mercado == 'MENOS DE 15 GOLS' && card.t == comparador ) || 
        (card.g <= 2 && card.mercado == 'MENOS DE 25 GOLS' && card.t == comparador ) ||
        (card.g <= 3 && card.mercado == 'MENOS DE 35 GOLS' && card.t == comparador ) ||
        (card.g <= 4 && card.mercado == 'MENOS DE 45 GOLS' && card.t == comparador ) ||
        (card.g <= 5 && card.mercado == 'MENOS DE 55 GOLS' && card.t == comparador ) ||
        (card.g <= 6 && card.mercado == 'MENOS DE 65 GOLS' && card.t == comparador ) ||
        (card.g <= 7 && card.mercado == 'MENOS DE 75 GOLS' && card.t == comparador ) ||
        (card.g <= 8 && card.mercado == 'MENOS DE 85 GOLS' && card.t == comparador )){ 
        card.finalizado = true;
        return "inplay-green";
      }
      return "inplay-white";
    }
  }

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Cockpit';
    
    config.map([
      { route: '', redirect:'cockpit'},
      { route: ['cockpit'], name:'cockpit', title: 'Cockpit',  moduleId: PLATFORM.moduleName('views/cockpit/cockpit'), settings: { auth: true, local:"Cockpit" }},
      { route: ['ajuda'], name:'ajuda', title: 'Ajuda',  moduleId: PLATFORM.moduleName('views/ajuda/ajuda'), settings: { auth: true, local:"Cockpit" }},
      
    ]);
    config.mapUnknownRoutes({ redirect: 'nao-encontrado' });
  }


  onSetProgressVolume = (type, status, obj) => {
    const floor = Math.floor

    if (!status) return 0;
    if (obj == 'plus') {
      let { hso, gso, hsf, gsf, hd, gd, ha, ga, hqq, gqq } = status;
      hso = floor(hso)
      gso = floor(gso)
      hsf = floor(hsf)
      gsf = floor(gsf)
      hd = floor(hd)
      gd = floor(gd)
      ha = floor(ha)
      ga = floor(ga)
      hqq = floor(hqq)
      gqq = floor(gqq)
      switch (type) {
        case 'nogol':
          return this.ruleOfThree(hso, gso)
        case 'pfora':
          return this.ruleOfThree(hsf, gsf)
        case 'ataquep':
          return this.ruleOfThree(hd, gd)
        case 'ataque':
          return this.ruleOfThree(ha, ga)
        case 'posse':
          return this.ruleOfThree(hqq, gqq)
        default:
          return 0;
      }
    } else {
      let { hc, gc } = status;
      hc = floor(hc)
      gc = floor(gc)
      switch (type) {
        case 'esc':
          return this.ruleOfThree(hc, gc)
        default:
          hc
      }
    }
    return 0;
  }

  itMustBeShown = (type, status, obj) => {
    const floor = Math.floor
    if (!status) {
      return false;
    }
     
    if (obj == 'plus') {
      let { hso, gso, hsf, gsf, hd, gd, ha, ga, hqq, gqq } = status;
      hso = floor(hso)
      gso = floor(gso)
      hsf = floor(hsf)
      gsf = floor(gsf)
      hd = floor(hd)
      gd = floor(gd)
      ha = floor(ha)
      ga = floor(ga)
      hqq = floor(hqq)
      gqq = floor(gqq)
      switch (type) {
        case 'nogol':
          return (hso+gso>0) ? true : false;
        case 'pfora':
          return (hsf+gsf>0) ? true : false;
        case 'ataquep':
          return (hd+gd>0) ? true : false;
        case 'ataque':
          return (ha+ga>0) ? true : false;
        case 'posse':
          return (hqq+gqq>0) ? true : false;
        default:
          return false;
      }
    } else {
      let { hc, gc } = status;
      hc = floor(hc)
      gc = floor(gc)
      switch (type) {
        case 'esc':
          return (hc+gc>0) ? true : false;
        default:
          return false;
      }
    }
  }

  ruleOfThree = (home, visit) => {
    const total = home + visit
    return (total > 0) ? (Math.floor((home * 100) / total)) : 0
  }
}
