import {BasicView} from 'views/BasicView';

export class Atualizar extends BasicView {
  
  constructor(...rest){
    super(...rest);
  }  
    
  attached(){
    this.buscarTimes();
  }

  async atualizar(item){
    let v = {id:item.id, value:item.roboTip}
    let retoro = await this.service.atualizarRoboTip(v);
  }

  async buscarTimes(){
    let retorno  = await this.service.buscarTimesRoboTip();
    this.times =  retorno.value;
  }
}
  