import {BasicView} from 'views/BasicView';

export class Teste extends BasicView {
  constructor(...rest){
    super(...rest);
  }  

  attached(){
    $('.modal-backdrop.fade.show').remove();
  }
  
  onVoltarLogin(){
    this.navigate("login");
  }
}
