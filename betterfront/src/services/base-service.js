
export class BaseService {

  constructor(eventAggregator, BASE_URL, http, storageUtil) {
    this.ea = eventAggregator;
    this.baseUrl = BASE_URL;
    this.http = this.configureHttpBasicUrl(http);
    this.session = storageUtil;
  }

  get(url, parameters=null) {
    if (parameters!==null){
      let query = Object.keys(parameters).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(parameters[k])).join('&');
      url = url + '?'+query;
    }
    return new Promise((resolve, reject) => {
      this.http 
        .fetch(url, {
          method: "GET"
        })        
        .then(response => response.json())
        .then(response => resolve(this.getResponse(response, 'value')))
        .catch(error => reject(this.errorHandler(error)));
    });
  }

  postString(obj, url) {
    if (!url)
      url = "";

    return new Promise((resolve, reject) => {
      this.http
        .fetch( url, {
          method: "POST",
          body: obj
        })
        .then(response => response.json())
        .then(response => resolve(response))
        .catch(error => reject(this.errorHandler(error)));
    });
  }

  put(obj, url) {
    if (!url)
      url = "";

    return new Promise((resolve, reject) => {
      this.http
        .fetch(url, {
          method: "PUT",
          body: JSON.stringify(obj)
        })
        .then(response => response.json())
        .then(response => resolve(this.getResponse(response, 'value')))
        .catch(error => reject(this.errorHandler(error)));
    });
  }

  post(obj, url) {
    if (!url)
      url = "";

    return new Promise((resolve, reject) => {
      this.http
        .fetch(url, {
          method: "POST",
          body: JSON.stringify(obj)
        })
        .then(response => response.json())
        .then(response => resolve(this.getResponse(response, 'value')))
        .catch(error => reject(this.errorHandler(error)));
    });
  }

  async errorHandler(error){
    this.ea.publish("ERRO_REQUEST",error);
    return error;
  }

  configureHttpBasicUrl(http) {
    var _this = this;
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl(this.baseUrl) 
        .withDefaults({
          headers: {
            Accept: "application/json",
            "X-Requested-With": "Fetch",
            "Content-Type": "application/json"
          }
        })
        .withInterceptor({
             request(request) {
               let user = _this.session.get('user');
               if (user !== null)
                 request.headers.append('Authorization', 'Bearer '+user.token);
                 return request;
             } 
         });
    });
    return http;
  }

  getResponse(response, property) {
     
    if(response.status.code == -1){
      this.ea.publish("SESSION_EVT",{code:"FORBIDDEN"});
    }
    if(response.status.code == -2){
      this.ea.publish("SESSION_EVT",{code:"EXPIRED"});
    }
    return response;
  }

  throwError(response){
    throw response;
  }

}
