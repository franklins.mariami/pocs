import {inject, NewInstance} from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import {BaseService} from "./base-service";
import {EventAggregator} from 'aurelia-event-aggregator';
import environment from '../../config/environment.json';
import {StorageUtil} from "util/storage-util";

@inject(NewInstance.of(HttpClient), EventAggregator,StorageUtil)
export class Service extends BaseService{

  constructor(http, eventAggregator, storageUtil){
    super(eventAggregator, environment.baseUrl, http, storageUtil);
  } 

  salvarScorebing(dados){
    return this.post(dados,"/cockpit/salvar-sb");
  }

  atualizarRoboTip(dados){
    return this.post(dados,"/external/update-rt");
  }

  aumentarStack(dados){
    return this.post(dados,"/cockpit/aumentar-stack");
  }

  buscarTimesRoboTip(){
    return this.get("/external/time/tip/");
  }

  buscarOddPorEntrada(idEntrada){ 
    return this.get("/cockpit/odd/" + idEntrada);
  }  


  removerFavorito(id){
    return this.post({"id":id},"/favorito/remover");
  }

  favoritar(times){
    return this.post({"times":times},"/favorito/favoritar");
  }
  
  buscarFavorito(){
    return this.get("/favorito/pesquisar");
  }

  pesquisarTime(nome){
    return this.post({"nome":nome},"/time/pesquisar");
  }
 
  buscarStatusClienteCockpit(){ 
    return this.get("/cockpit/status-cliente-cockpit");
  } 

  entradaManual(dados){
    return this.post(dados,"/cockpit/entrada-manual");
  }

  buscarSaldoConta(){
    return this.get("/cockpit/saldo-conta");
  }

  encerrarPartida(dados){
    return this.post(dados,"/cockpit/encerrar-partida");
  }

  removerPartida(dados){
    return this.post(dados,"/cockpit/remover-partida");
  }

  atualizarDadosCliente(dados){
    return this.post(dados,"/client/update");
  }

  buscarStatusCliente(dados){ 
    return this.post(dados,"/client/status");
  } 

  loginCliente(login,senha){ 
    return this.post({"login":login,"senha":senha},"/login/in-cliente");
  } 

  abrirTips(){
    return this.get("/cockpit/abrir-tips");
  }

  habilitarRoboAutomatico(idConta){
    return this.post({"idConta":idConta},"/cockpit/habilitar-robo");
  }

  cadastrarContaExternaCockpit(dados){
    return this.post(dados,"/external/cockpit");
  }
  
  cadastrarContaCockpit(dados){
    return this.post(dados,"/cockpit/cadastrar-conta");
  }

  enviarDuvida(dados){ 
    return this.post(dados,"/cockpit/duvida");
  } 

  update(dados){ 
    return this.post({"data":dados},"/cockpit/update");
  } 

  enviarSinal(dados){
    return this.post(dados,"/cockpit/enviar-tip");
  }
  excluirConta(dados){
    return this.post(dados,"/cockpit/excluir-conta");
  }

  ativarConta(dados){
    return this.post(dados,"/cockpit/ativar-conta");
  }

  buscarConta(idLogin){
    return this.get("/cockpit/conta/" + idLogin);
  }

  atualizarContaCockpit(dados){
    return this.post(dados,"/cockpit/atualizar-conta");
  }

  cadastrarContaCockpit(dados){
    return this.post(dados,"/cockpit/cadastrar-conta");
  }
  
  testarConta(dados){
    return this.post(dados,"/cockpit/testar-conta");
  }

  buscarLogins(){
    return this.get("/cockpit/logins");
  }
  
  cashOut(idPartida, idMercado,idSelection){
    return this.post({},"/cockpit/cashout/"+idPartida+ "/" + idMercado + "/" + idSelection);
  }

  buscarEntradasRealizadas(idPartida, idMercado,idSelection){
    return this.get("/cockpit/entradas/" + idPartida+ "/" + idMercado + "/" + idSelection);
  }

  searchPanelFuturo(data){ 
    return this.post({"data":data},"/cockpit/search-futuro");
  } 
  atualizarDadosPartida(dados){
    return this.post({"partidas":dados},"/cockpit/atualizar-partidas");
  }

  buscarOperacoes(idLogin){
    return this.get("/cockpit/operacoes/" + idLogin);
  }

  buscarOportunidadeOver15(dados){
    return this.post({"partidas":dados},"/cockpit/partida-potencial-over15");
  }

  atualizarTime(dados){
    return this.put(dados,"/external/time");
  }
  
  buscarTimesConfere(){ 
    return this.get("/external/conferir-times");
  } 

  buscarTimes(){ 
    return this.get("/external/times");
  }  

  cadastrarContaCliente(dados){
    return this.post(dados,"/external/cliente");
  }

  buscarOdd(mercado, operador,idPartida){ 
    return this.get("/cockpit/odd/" + mercado + '/' + operador + '/' + idPartida);
  }  

  doCopyAll(dados){
    return this.post(dados,"/cockpit/copy-all");
  }

  doCopy(dados){
    return this.post(dados,"/cockpit/copy");
  }

  ativarConta(dados){
    return this.post(dados,"/cockpit/ativar-conta");
  }
   
  buscarContasCopy(){ 
    return this.get("/cockpit/contas-copy");
  }  

  buscarContas(){ 
    return this.get("/cockpit/contas");
  }  

  buscarInPlay(){ 
    return this.get("/cockpit/inplay"); 
  } 
  
  buscarLucro(){ 
    return this.get("/cockpit/lucro");
  } 

  login(login,senha){ 
    return this.post({"login":login,"senha":senha},"/login/in");
  } 

  searchPanel(data){ 
    return this.post({"data":data},"/cockpit/search");
  } 

  search(home, visitor){ 
    return this.post({"home":home, "visitor":visitor},"/academia/search");
  }

  finalizarAtendimento(id){
    return this.postString(id, "/externo/atendimento/finalizar");
  }

  processPanel(){
    return this.get("/cockpit/process");
  }
  
}
