import {PLATFORM} from 'aurelia-pal';
export function configure(config) {
  config.globalResources([
    PLATFORM.moduleName('./attributes/on-enter-key'),
    PLATFORM.moduleName('./slider-double/slider-double'),
  ]);
}
