import {BasicView} from 'views/BasicView';

export class Topbar  extends BasicView {

  constructor(...rest){
    super(...rest);
  }  
   
  onLogout = () => {
    this.navigate("/login-cliente-du");
  }
}
