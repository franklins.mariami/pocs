import {bindable} from 'aurelia-framework';

export class PressureFlag {
  @bindable home = false;
  @bindable homeon = false;
  @bindable away = false;
  @bindable awayon = false;
}
