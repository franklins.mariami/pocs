import environment from '../config/environment.json';
import {PLATFORM} from 'aurelia-pal';
import {I18N, TCustomAttribute} from 'aurelia-i18n';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'))
    .plugin(PLATFORM.moduleName('aurelia-dialog'))
    .globalResources([
      PLATFORM.moduleName('views/cockpit/live/lucro-diario/lucro-diario'),
      PLATFORM.moduleName('views/cockpit/live/ajuda/ajuda'),
      PLATFORM.moduleName('views/cockpit/live/favorito/favorito'),
      PLATFORM.moduleName('views/cockpit/live/contas/contas'),
      PLATFORM.moduleName('resources/pressure-flag/pressure-flag'),
      PLATFORM.moduleName('resources/layout/topbar/topbar'),
      PLATFORM.moduleName('resources/layout/footer-component/footer-component'),
    ]);

    //aurelia.use.plugin(PLATFORM.moduleName('aurelia-i18n'), (instance) => {
    //  let aliases = ['t', 'i18n'];
    //  TCustomAttribute.configureAliases(aliases);
    //  instance.i18next.use(Backend);
  //
    //  return instance.setup({
    //    backend: {
    //      loadPath: './locales/{{lng}}/{{ns}}.json',
    //    },
    //    attributes: aliases,
    //    lng : 'pt',
    //    fallbackLng : 'en',
    //    debug : false
    //  });
    //});

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
