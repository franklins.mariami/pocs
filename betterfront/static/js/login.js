/*==================================================================
[ Validate after type ]*/
$('.validate-input .input100').each(function(){
    $(this).on('blur', function(){
        if(validate(this) == false){
            showValidate(this);
        }
        else {
            $(this).parent().addClass('true-validate');
        }
    })    
})

/*==================================================================
[ Validate ]*/
var input = $('.validate-input .input100');

$('.validate-form').on('submit',function(){
    var check = true;

    for(var i=0; i<input.length; i++) {
        if(validate(input[i]) == false){
            showValidate(input[i]);
            check=false;
        }
    }

    return check;
});


$('.validate-form .input100').each(function(){
    $(this).focus(function(){
        hideValidate(this);
        $(this).parent().removeClass('true-validate');
    });
});

function validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');

    $(thisAlert).append('<span class="btn-hide-validate">&#xf135;</span>')
    $('.btn-hide-validate').each(function(){
        $(this).on('click',function(){
            hideValidate(this);
        });
    });
}

function hideValidate(input) {
    var thisAlert = $(input).parent();
    $(thisAlert).removeClass('alert-validate');
    $(thisAlert).find('.btn-hide-validate').remove();
}
    


/*==================================================================
[ open forgot password modal ]*/
const dialog = document.querySelector('dialog'),
invoke = document.getElementById('invoke'),
output = document.querySelector('output'),
cancel = dialog.querySelector('.cancel');

if (typeof dialog.showModal !== 'function') {
    output.value = `Modal dialog not supported`;
}

invoke.addEventListener('click', () => {
    dialog.returnValue = false;
    dialog.showModal();
});

dialog.addEventListener('close', () => {
    dialog.returnValue = dialog.returnValue.toLowerCase() === 'true';
    output.value = dialog.returnValue;
});

cancel.addEventListener('click', () => {
    dialog.close(false);
});

dialog.addEventListener('click', (e) => {
    const rect = dialog.getBoundingClientRect();

    const inDialog =
        rect.top <= e.clientY &&
        e.clientY <= rect.top + rect.height &&
        rect.left <= e.clientX &&
        e.clientX <= rect.left + rect.width;

    !inDialog && dialog.close();
});