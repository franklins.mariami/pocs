const progressbar = document.querySelectorAll(".progress")
const goalBallIn = document.createElement('img')
const goalBallOut = document.createElement('img')
goalBallIn.src = 'images/icons/ball1.png'
goalBallOut.src = 'images/icons/ball2.png'
goalBallIn.classList.add('fa-ball-sm', 'fa-ball-in')
goalBallOut.classList.add('fa-ball-sm', 'fa-ball-out')

const changeProgress = () => {
  removeAllGols()
  tableItems.forEach(match =>  {
    const firstHalf = [...document.querySelectorAll(`.progress-first-half-id-${match.id}`)];
    const secondHalf = [...document.querySelectorAll(`.progress-second-half-id-${match.id}`)];

    firstHalf.map(matchProgressElement => {
      fillProgress(match.currentTime, matchProgressElement, false)
      fillGoal(matchProgressElement, match.goalsIn, match.goalsOut, false)
    })
    secondHalf.map(matchProgressElement => {
      fillProgress(match.currentTime, matchProgressElement, true)
      fillGoal(matchProgressElement, match.goalsIn, match.goalsOut, true)
    })


    // fillGoal(match.currentTime, firstHalf, secondHalf, match.goalsIn, match.goalsOut)
  })
}

const removeAllGols = () => {
  document.querySelectorAll('.fa-ball-sm').forEach(box => {
    box.remove()
  })
}

const fillProgress = (time, matchProgressElement, isSecond) => {
  if (isSecond) {
    matchProgressElement.style.width = `${combineValues(time - 45)}%`
  } else {
    if (time > 45) {
      matchProgressElement.style.width = "100%"
    } else {
      matchProgressElement.style.width = `${combineValues(time)}%`
    }
  }
}
const fillGoal = (matchProgressElement, goalsIn, goalsOut, isSecond) => {
  let matchProgressParent = matchProgressElement.parentNode

  if (isSecond) {
    goalsIn.forEach(goal => {
      if (goal > 45) {
        const clone = goalBallIn.cloneNode(true)
        if (goal > 90) goal = 90
        let newTime = goal - 45
        clone.style.left = `${combineValuesGoal(newTime, matchProgressParent.offsetWidth)}px`
        matchProgressParent.appendChild(clone)
      }
    })
    goalsOut.forEach(goal => {
      if (goal > 45) {
        const clone = goalBallOut.cloneNode(true)
        if (goal > 90) goal = 90
        let newTime = goal - 45
        clone.style.left = `${combineValuesGoal(newTime, matchProgressParent.offsetWidth)}px`
        matchProgressParent.appendChild(clone)
      }
    })
  } else {
    goalsIn.forEach(goal => {
      if (goal <= 45) {
        const clone = goalBallIn.cloneNode(true)
        clone.style.left = `${combineValuesGoal(goal, matchProgressParent.offsetWidth)}px`
        matchProgressParent.appendChild(clone)
      }
    })
  
    goalsOut.forEach(goal => {
      if (goal <= 45) {
        const clone = goalBallOut.cloneNode(true)
        clone.style.left = `${combineValuesGoal(goal, matchProgressParent.offsetWidth)}px`
        matchProgressParent.appendChild(clone)
      }
    })
  }
}

// const isOdd = (num) => {
//   return (num % 2 === 1) ? true : false
// }

const getInnerWidth = (elem) => {
  return parseFloat(window.getComputedStyle(elem).width)
}

combineValues = (time) => {
  return Math.trunc(100*time/45)
}

combineValuesGoal = (time, elWidth) => {
  let initialValue = -7 //0px
  let multiplier = elWidth/45
  return Math.trunc(initialValue + (time * multiplier))
}

setTimeout(() => changeProgress(), 1000)



// BEGIN - Set debounce on window resize
let timeout = false
let delay = 250
let calls = 0
let matchTable = document.getElementById("match-table")
const outputsize = () => {
  clearTimeout(timeout)
  timeout = setTimeout(changeProgress, delay)
}
new ResizeObserver(outputsize).observe(matchTable)
// END - Set debounce on window resize