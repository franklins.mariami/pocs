// import { changeProgress } from './progress-bar'

// const dialogOdds = document.getElementById('modal-odds');
// const cancelDialogOdds = dialogOdds.querySelector('.cancel');
const modalAddOdds = new bootstrap.Modal(document.getElementById('modal-example'), {})

const tableItems = [
  {
    id: 1,
    country: "Italy",
    championship: "Second Division",
    currentTime: 75,
    home: "Juventus Sub19",
    scoreboard: "1-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [15, 50],
    goalsOut: [20, 55, 62, 66]
  },
  {
    id: 2,
    country: "Italy",
    championship: "Second Division",
    currentTime: 77,
    home: "Juventus Sub19",
    scoreboard: "0-1",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [1],
    goalsOut: [10]
  },
  {
    id: 3,
    country: "Italy",
    championship: "Second Division",
    currentTime: 70,
    home: "Juventus Sub19",
    scoreboard: "1-1",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [46],
    goalsOut: [55]
  },
  {
    id: 4,
    country: "Italy",
    championship: "Second Division",
    currentTime: 35,
    home: "Juventus Sub19",
    scoreboard: "2-1",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [25],
    goalsOut: [10]
  },
  {
    id: 5,
    country: "Italy",
    championship: "Second Division",
    currentTime: 45,
    home: "Juventus Sub19",
    scoreboard: "4-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 6,
    country: "Italy",
    championship: "Second Division",
    currentTime: 46,
    home: "Juventus Sub19",
    scoreboard: "0-5",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 7,
    country: "Italy",
    championship: "Second Division",
    currentTime: 47,
    home: "Juventus Sub19",
    scoreboard: "9-2",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 8,
    country: "Italy",
    championship: "Second Division",
    currentTime: 48,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 9,
    country: "Italy",
    championship: "Second Division",
    currentTime: 49,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 10,
    country: "Italy",
    championship: "Second Division",
    currentTime: 50,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 11,
    country: "Italy",
    championship: "Second Division",
    currentTime: 0,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 12,
    country: "Italy",
    championship: "Second Division",
    currentTime: 89,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 13,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 14,
    country: "Italy",
    championship: "Second Division",
    currentTime: 90,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 15,
    country: "Italy",
    championship: "Second Division",
    currentTime: 91,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 16,
    country: "Italy",
    championship: "Second Division",
    currentTime: 46,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 17,
    country: "Italy",
    championship: "Second Division",
    currentTime: 47,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 18,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 19,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 20,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 21,
    country: "Italy",
    championship: "Second Division",
    currentTime: 91,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 22,
    country: "Italy",
    championship: "Second Division",
    currentTime: 46,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 17,
    country: "Italy",
    championship: "Second Division",
    currentTime: 47,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 23,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 24,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 25,
    country: "Italy",
    championship: "Second Division",
    currentTime: 32,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  },
  {
    id: 26,
    country: "Italy",
    championship: "Second Division",
    currentTime: 91,
    home: "Juventus Sub19",
    scoreboard: "0-0",
    visitant: "Liverpool Sub19",
    startTime: "12:15",
    odds1: "6.4",
    odds2: "6.4",
    goalsIn: [],
    goalsOut: []
  }
]

const loadTableData = (items) => {
  const table = document.getElementById("table-body")
  items.forEach(item => {
    let row = table.insertRow()

    let favorite = row.insertCell(0)
    favorite.innerHTML = `
      <a class="cursor-pointer" onclick="onToggleFavoriteMatch(event)">
        <i class="fa-solid fa-star fa-header-custom" ></i>        
      </a>
    `

    let startTime = row.insertCell(1)
    startTime.innerHTML = item.startTime

    let country = row.insertCell(2)
    country.innerHTML = item.country
    
    let championship = row.insertCell(3)
    championship.innerHTML = item.championship

    let firstHalf = row.insertCell(4)
    firstHalf.innerHTML = `
    <div class="progress-container">
      <div class="progress progress-first-half-id-${item.id}"></div>
    </div>
    `
    firstHalf.className = "first-half"
    
    let currentTime = row.insertCell(5)
    currentTime.innerHTML = `${item.currentTime}'`
    currentTime.className = "current-time"

    let secondtHalf = row.insertCell(6)
    secondtHalf.innerHTML = `
    <div class="progress-container">
    <div class="progress progress-second-half-id-${item.id}"></div>
    </div>
    `
    secondtHalf.className = "second-half"

    let home = row.insertCell(7)
    home.innerHTML = item.home
    home.className = "text-right"

    let scoreboard = row.insertCell(8)
    scoreboard.innerHTML = item.scoreboard
    scoreboard.className = "scoreboard"

    let visitant = row.insertCell(9)
    visitant.innerHTML = item.visitant
    visitant.className = "text-left"
    
    let odds1 = row.insertCell(10)
    odds1.innerHTML = item.odds1
    odds1.className = "match-odds"
    
    let odds2 = row.insertCell(11)
    odds2.innerHTML = item.odds2
    odds2.innerHTML = `
      <a class="cursor-pointer" onclick="onOpenOdds(event)">
        <i class="fa-solid fa-futbol" style="font-size: 1rem;" ></i>        
      </a>
    `
  })
}

// const onSelectTab = (e, clickedTab) => {
//   const tabs = document.querySelectorAll('.tab-area')
//   const tabBtns = document.querySelectorAll('.tab button')
//   const clickedBtn = e.target

//   tabBtns.forEach(btn => {
//     if (clickedBtn === btn) btn.classList.add('selected')
//     else btn.classList.remove('selected')
//   })

//   tabs.forEach((tab, index) => {
//     if (index === clickedTab) {
//       tab.classList.remove('d-none')
//       tab.classList.add('d-flex')
//     }
//     else {
//       tab.classList.remove('d-flex')
//       tab.classList.add('d-none')
//     }
//   })
// }

const onToggleFollowingArea = () => {
  const fAreaOpened = document.getElementById('following-area-opened')
  const fAreaClosed = document.getElementById('following-area-closed')

  fAreaOpened.classList.toggle('d-none')
  fAreaClosed.classList.toggle('d-none')
}

const onToggleFavoriteMatch = (e) => {
  console.log(e.target)
  e.target.classList.toggle('fa-star-favorite')

  let newFav = document.createElement("div")
  newFav.classList.add("match-card", 'animate__animated', 'animate__bounceIn')
  newFav.innerHTML = `
    <div class="match-card-top">
      <div class="info-top">
        <span>6.4</span>
        <i class="fa-solid fa-star fa-following-custom cursor-pointer" onclick="onRemoveFromFavorites(event)"></i>
      </div>
      <div class="info-center">
        <span>Juventus Sub19</span>
        <span>0-0</span>
        <span>Liverpool Sub19</span>
      </div>            
    </div>
    <div class="match-card-bottom">
      <div class="progress-container pc-big">
        <div class="progress progress-first-half-id-1"></div>
      </div>
      <span class="px-1">75'</span>
      <div class="progress-container pc-big">
        <div class="progress progress-second-half-id-1"></div>
      </div>
    </div>
  `

  let favList = document.getElementById("following-matches")
  favList.appendChild(newFav)

  // Somente para ilustrar a adição de card com conteúdo na lista de acompanhamento de partidas
  jQuery.getScript("js/progress-bar.js", () => {
    changeProgress();
  });
}

const onRemoveFromFavorites = e => {
  const parentWithClass = e.target.closest(".match-card")
  parentWithClass.classList.add('animate__animated', 'animate__bounceOut')
  setTimeout(() => {
    parentWithClass.remove()
  }, 1500);
}

const onOpenOdds = (e) => {
  console.log(e.target)
  modalAddOdds.show()
}

const onChooseOdds = (e) => {
  const elems = [...document.querySelectorAll('.odds-odds')]
  const elem = e.target
  elems.forEach(element => {
    element.classList.remove('selected')
  });
  elem.classList.toggle('selected')
}

const onConfirmOdds = () => {
  const btnClose = document.getElementById('btn-odds-close')
  btnClose.click();
}

const placeholderFunction = () => {
  console.log('clicked')
}

const onAnimateScore = () => {
  const elements = [...document.querySelectorAll('.scoreboard')]
  const selectElement = elements[getRandomInt(0, elements.length -1)]
  selectElement.classList.add('animate__animated', 'animate__heartBeat', 'scoreboard-animated')
  setTimeout(() => {
    selectElement.classList.remove('animate__animated', 'animate__heartBeat', 'scoreboard-animated')
  }, 10000);
  // selectElement.classList.add('score-changed')
  // setTimeout(() => {
  //   selectElement.classList.remove('score-changed')
  // }, 10000);
}

const onAnimateOdds = () => {
  const elements = [...document.querySelectorAll('.match-odds')]
  const selectElement = elements[getRandomInt(0, elements.length -1)]
  selectElement.classList.add('animate__animated', 'animate__heartBeat', 'animate__repeat-2', 'odd-animated')
  setTimeout(() => {
    selectElement.classList.remove('animate__animated', 'animate__heartBeat', 'animate__repeat-2', 'odd-animated')
  }, 6000);
}

const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}


// loadTableData(tableItems)
// setInterval(() => {
//   onAnimateScore()
// }, 6000)
// setInterval(() => {
//   onAnimateOdds()
// }, 2500);
