package ead.video;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Panel extends JFrame {
	private JPanel painel = null;

	private JButton botao = null;

	private JLabel label = null;

	private Container container = null;

	public static void main(String[] args) {
		new Panel();
	}

	public Panel() {
		setSize(400, 200);
		label = new JLabel("Painel");
		container = getContentPane();
		container.setLayout(null);

		botao = new JButton("Bot�o no painel");
//		botao.setBounds(180, 140, 40, 20);
		botao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Compress.run();
			}
		});
		painel = new JPanel();
		painel.setBackground(Color.white);
		painel.add(label);
		painel.add(botao);
		painel.setBounds(280, 1, 200, 50);

		container.add(painel);

		setLocation(200, 50);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
