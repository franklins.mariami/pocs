package ead.video;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;

public class Compress {

	private static final long  MEGABYTE = 1024L * 1024L;
	
	public static void main(String[] args) {
//		
//		if(args[1].equals("dev")) {
//			doCompress(args[0], true); 
//		}else {
//			doCompress(args[0], false);
//		}
//		System.out.println(formatTempo((10000l)));
//		run();  
	}
	
	public static void run() {
//		encript("C:/desenv/shaka/video.mp4");
	}
 
	
	private static long bytesToMeg(long bytes) {
		  return bytes / MEGABYTE ;
	}
	
	public  static String doCompress(String from) {
		return doCompress(from,null);
	}
	
	public static Boolean encript(String from) {
		try { 
			ProcessBuilder processBuilder = new ProcessBuilder();

			System.out.println("SHAKA :: PROTEGENDO VIDEO " + from);

			File f = new File(from);
			if(f.exists()) {
				Boolean win = false;
				String servico = "/usr/local/bin/shaka-packager ";
				String op = System.getProperty("os.name");
				if(op.toLowerCase().indexOf("windows") != -1) {
					servico = "C:/desenv/shaka/packager-win.exe ";
					win = true;
				} 
				
				String nomeAudio = f.getName().substring(0, f.getName().lastIndexOf(".")) + "_audio.mp4";
				String pathAudio = f.getParent() + File.separator+ nomeAudio;  
				
				String pathMpd = f.getPath().replace(".mp4", ".mpd");
				
				String urlBuild = "input=" +from + ",stream=audio,output="+pathAudio+",drm_label=AUDIO input=" + from + ",stream=video,output="+ from +",drm_label=SD  --enable_raw_key_encryption --keys label=AUDIO:key_id=f3c5e0361e6654b28f8049c778b23946:key=a4631a153a443df9eed0593043db7519,label=SD:key_id=abba271e8bcf552bbd2e86a434a9a5d9:key=69eaa802a6763af979e8d1940fb88392 --generate_dash_if_iop_compliant_mpd=false --mpd_output " + pathMpd;
				
				String comando = servico + urlBuild;
				System.out.println("COMANDO.: " + comando);
				
				if(!win) {
					processBuilder.command("bash", "-c", comando);
				}else {
					processBuilder.command(comando);
				}
		        Process process = processBuilder.start();
		        StringBuilder output = new StringBuilder();

		        BufferedReader reader = new BufferedReader( new InputStreamReader(process.getInputStream()));

		        String line;
		        while ((line = reader.readLine()) != null) {
		            output.append(line + "\n");
		        }
		        int exitVal = process.waitFor();
		        if (exitVal == 0) {
		        	System.out.println("SHAKA :: CRIPTOGRAFIA REALIZADA");
		            System.out.println(output);
		        } else {
		        	System.out.println("SHAKA :: ALGO DEU ERRADO COM A CRIPTOGRAFIA");
		        }
		        return true;
			}else {
				System.out.println("SHAKA :: VIDEO NAO ENCONTRADO ");
				return false;
			}
			
		} catch (Exception e) {
			System.out.println("SHAKA :: ERRO NA CRIPTOGRAFIA");
			e.printStackTrace();
			return false;
		}
	} 
	
	public  static String doCompress(String from, String watermark) {
		String novoTamanho = null;
		try {
			FFmpeg ffmpeg = null;
//			FFprobe ffprobe = null;
			String op = System.getProperty("os.name");
			if(op.toLowerCase().indexOf("windows") != -1) {
				ffmpeg = new FFmpeg("C:\\desenv\\ffmpeg\\ffmpeg.exe");
//				ffprobe = new FFprobe("D:\\databit\\ffmpeg\\bin\\ffprobe.exe");
			}else {
				ffmpeg = new FFmpeg("/usr/bin/ffmpeg");
//				ffprobe = new FFprobe("/usr/bin/ffmpeg");
			}
//			FFmpegProbeResult probeResult = ffprobe.probe(from);
			
			String to = new File(from).getParent() + File.separator+ "START_WITH_OLD_" + new File(from).getName();
			Long tempo = System.currentTimeMillis();
			
			StringBuffer buf = new StringBuffer();
			buf.append("COMPRESS :: FILE ").append(from).append("\n");
			String tamanhoReal = bytesToMeg(new File(from).length()) + " MB";
			
			buf.append("COMPRESS :: SIZE ATUAL ").append(tamanhoReal).append("\n");
			
			FFmpegBuilder builder = new FFmpegBuilder().setInput(from);
			 
			if(watermark != null) {
				buf.append("COMPRESS :: UTILIZANDO MARCADGUA").append(watermark).append("\n");
				builder.addInput(watermark).setComplexFilter("overlay=5:5");
			}
			System.out.println(buf);
			builder.overrideOutputFiles(true) // Override the output if it exists
			  .addOutput(to)   // Filename for the destination
			    .setFormat("mp4")        // Format is inferred from filename, or can be set
			    .disableSubtitle()       // No subtiles
			    .setAudioChannels(1)         // Mono audio
			    .setAudioCodec("aac")        // using the aac codec
			    .setAudioSampleRate(net.bramp.ffmpeg.FFmpeg.AUDIO_SAMPLE_48000)  // at 48KHz
			    .setAudioBitRate(32768)      // at 32 kbit/s
			    .setVideoCodec("libx264")     // Video using x264
			    .setVideoFrameRate(24, 1)     // at 24 frames per second
			    .setVideoResolution(740, 480) // at 640x480 resolution
			    .setVideoQuality(100.0)
			    .setStrict(FFmpegBuilder.Strict.EXPERIMENTAL) // Allow FFmpeg to use experimental specs
			    .done();
			
			FFmpegExecutor executor = new FFmpegExecutor(ffmpeg);

			// Run a one-pass encode
			executor.createJob(builder).run();
//			probeResult = ffprobe.probe(to);
			
			//renomeia o original
			File atual = new File(from);
			
			Long tamanhoOriginal = new File(from).length();
			Long tamanhoConvertido = new File(to).length();
			
			if(tamanhoConvertido > tamanhoOriginal && watermark == null) {
				buf.append("COMPRESS :: ABORTANDO CONVERSAO TAMANHO MAIOR.: ").append("\n");
				org.apache.tools.ant.util.FileUtils.delete(new File(to));
			}else {
			
				File atualTemp = new File(from + "_temp");
				atual.renameTo(atualTemp);
				
				//renomeia o old para o original
				File convertido = new File(to);
				File original = new File(to.replace("START_WITH_OLD_", ""));
				convertido.renameTo(original);
				
				org.apache.tools.ant.util.FileUtils.delete(atualTemp);
				buf = new StringBuffer();
				buf.append("COMPRESS :: CONVERSAO TERMINADA - TAMANHO REAL ").append(tamanhoReal)
				   .append(" TAMANHO ATUAL ").append(bytesToMeg(new File(from).length()) + " MB").append("\n");
				
				novoTamanho = tamanhoReal; 
				
				executor = null;
			}
			Long total = System.currentTimeMillis() - tempo;
			buf.append("COMPRESS :: TEMPO GASTO ").append(formatTempo(total)).append("\n");
			System.out.println(buf.toString());
			
		} catch (Exception e) {
			if(!e.getMessage().startsWith("Target size, or video bitrate")) {
				e.printStackTrace();	
			}
			
		}
		return novoTamanho;
	}
	
	public static String formatTempo(Long durationInMillis) {
		long second = (durationInMillis / 1000) % 60;
		long minute = (durationInMillis / (1000 * 60)) % 60;
		long hour = (durationInMillis / (1000 * 60 * 60)) % 24;

		return  String.format("%02d:%02d:%02d", hour, minute, second);
	}
}
